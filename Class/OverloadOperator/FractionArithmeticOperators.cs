using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace playground.Class
{
    public readonly struct FractionArithmeticOperators
    {
        private readonly int num;
        private readonly int den;

        public FractionArithmeticOperators(int numerator, int denominator)
        {
            if (denominator == 0)
            {
                throw new ArgumentException("Denominator cannot be zero.", nameof(denominator));
            }
            num = numerator;
            den = denominator;
        }

        public static FractionArithmeticOperators operator +(FractionArithmeticOperators a) => a;
        public static FractionArithmeticOperators operator -(FractionArithmeticOperators a) => new FractionArithmeticOperators(-a.num, a.den);
        public static FractionArithmeticOperators operator +(FractionArithmeticOperators a, FractionArithmeticOperators b) => new FractionArithmeticOperators(a.num * b.den + b.num * a.den, a.den * b.den);
        public static FractionArithmeticOperators operator -(FractionArithmeticOperators a, FractionArithmeticOperators b) => a + (-b);
        public static FractionArithmeticOperators operator *(FractionArithmeticOperators a, FractionArithmeticOperators b) => new FractionArithmeticOperators(a.num * b.num, a.den * b.den);
        public static FractionArithmeticOperators operator /(FractionArithmeticOperators a, FractionArithmeticOperators b)
        {
            if (b.num == 0)
            {
                throw new DivideByZeroException();
            }
            return new FractionArithmeticOperators(a.num * b.den, a.den * b.num);
        }

        public override string ToString() => $"{num} / {den}";
    }
}