using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace playground.Class.OverloadOperator
{
    public class ComplexNumber : IComparable<ComplexNumber>
    {
        public int X { get; }
        public int Y { get; }

        public ComplexNumber(int x, int y)
        {
            X = x;
            Y = y;
        }

        // Implement the CompareTo method for ordering ComplexNumber
        public int CompareTo(ComplexNumber? other)
        {
            if (this.X > other?.X && this.Y > other.Y)
            {
                return 1;
            }
            if (this.X < other?.X && this.Y < other.Y)
            {
                return -1;
            }
            return 0;
        }

        public static bool operator <(ComplexNumber p1, ComplexNumber p2)
            => p1.CompareTo(p2) < 0;

        public static bool operator >(ComplexNumber p1, ComplexNumber p2)
            => p1.CompareTo(p2) > 0;

        public static bool operator <=(ComplexNumber p1, ComplexNumber p2)
            => p1.CompareTo(p2) <= 0;

        public static bool operator >=(ComplexNumber p1, ComplexNumber p2)
            => p1.CompareTo(p2) >= 0;

        public override string ToString() => $"({X}, {Y})";
    }
}