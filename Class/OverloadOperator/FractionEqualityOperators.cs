using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace playground.Class.OverloadOperator
{
    public readonly struct FractionEqualityOperators
    {
        private readonly int num;
        private readonly int den;

        public FractionEqualityOperators(int numerator, int denominator)
        {
            if (denominator == 0)
            {
                throw new ArgumentException("Denominator cannot be zero.", nameof(denominator));
            }
            num = numerator;
            den = denominator;
        }

        // Overload the equality operator (==)
        public static bool operator ==(FractionEqualityOperators a, FractionEqualityOperators b)
        {
            return a.num * b.den == b.num * a.den;
        }

        // Overload the inequality operator (!=)
        public static bool operator !=(FractionEqualityOperators a, FractionEqualityOperators b)
        {
            return !(a == b);
        }

        public override bool Equals(object? obj)
        {
            if (obj is FractionEqualityOperators other)
            {
                return this == other;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return (num, den).GetHashCode();
        }

        public override string ToString() => $"{num} / {den}";
    }
}