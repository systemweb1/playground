using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace playground.Class.Events
{
    public class CarDelegate
    {
        public delegate void CarEngineHandler(string msgForCaller); //the value type must the same as caller value

        // Now a public member!
        public CarEngineHandler? ListOfHandlers;

        // Just fire out the Exploded notification.
        public void Accelerate(int delta)
        {
            if (ListOfHandlers != null)
            {
                ListOfHandlers("Sorry, this car is dead...");
            }
        }
    }
}