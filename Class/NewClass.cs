// using System.Collections.Generic;

// public class Kata
// {
//   public static string AlphabetWar(string fight)
//   {
//     Dictionary<char, int> leftMap = new Dictionary<char, int>()
//     {
//       {'t', 0},
//       {'s', 1},
//       {'b', 2},
//       {'p', 3},
//       {'w', 4}
//     };

//     Dictionary<char, int> rightMap = new Dictionary<char, int>()
//     {
//       {'j', 0},
//       {'z', 1},
//       {'d', 2},
//       {'q', 3},
//       {'m', 4}
//     };

//     int leftScore = 0;
//     int rightScore = 0;
//     bool foundNextJ = false;
//     bool foundNextT = false;
//     bool foundPreviousJ = false;
//     bool foundPreviousT = false;
//     bool changedNext = false;

//     for (int i = 0; i < fight.Length; i++)
//     {
//       char letter = fight[i];
//       foundNextJ = false;
//       foundNextT = false;
//       foundPreviousJ = false;
//       foundPreviousT = false;

//       if (leftMap.ContainsKey(letter))
//       {
//         if (i < fight.Length - 2)
//         {
//           if (fight[i + 2] == 'j' || fight[i + 1] == 'j')
//           {
//             foundNextJ = true;
//           }
//         }

//         if (i > 1)
//         {
//           if (fight[i - 2] == 'j')
//           {
//             foundPreviousJ = true;
//           }
//         }

//         if (letter == 't')
//         {
//           changedNext = false;
//           if ((i == 0 && !leftMap.ContainsKey(letter)) || (i < fight.Length - 1 && rightMap.ContainsKey(fight[i + 1]) && !foundNextJ))
//           {
//             leftScore += rightMap[fight[i + 1]];
//             changedNext = true;
//           }

//           if (i > 0 && rightMap.ContainsKey(fight[i - 1]) && !foundPreviousJ)
//           {
//             leftScore += rightMap[fight[i - 1]];
//           }

//           if (changedNext)
//             i++;
//         }
//         else if (i == fight.Length - 1 || (i < fight.Length - 1 && fight[i + 1] != 'j') || (fight[i + 1] == 'j' && (i > 0 && fight[i - 1] == 't')))
//         {
//           leftScore += leftMap[letter];
//         }
//       }

//       if (rightMap.ContainsKey(letter))
//       {
//         if (i < fight.Length - 2)
//         {
//           if (fight[i + 2] == 't' || fight[i + 1] == 't')
//           {
//             foundNextT = true;
//           }
//         }

//         if (i > 1)
//         {
//           if (fight[i - 2] == 't')
//           {
//             foundPreviousT = true;
//           }
//         }

//         if (letter == 'j')
//         {
//           changedNext = false;
//           if ((i == 0 && !rightMap.ContainsKey(letter)) || (i < fight.Length - 1 && leftMap.ContainsKey(fight[i + 1]) && !foundNextT))
//           {
//             rightScore += leftMap[fight[i + 1]];
//             changedNext = true;
//           }

//           if (i > 0 && leftMap.ContainsKey(fight[i - 1]) && !foundPreviousT)
//           {
//             rightScore += leftMap[fight[i - 1]];
//           }

//           if (changedNext)
//             i++;
//         }
//         else if (i == fight.Length - 1 || (i < fight.Length - 1 && fight[i + 1] != 't') || (fight[i + 1] == 't' && (i > 0 && fight[i - 1] == 'j')))
//         {
//           rightScore += rightMap[letter];
//         }
//       }
//     }

//     //total score
//     if (leftScore > rightScore)
//     {
//       return "Left side wins!";
//     }
//     else if (leftScore < rightScore)
//     {
//       return "Right side wins!";
//     }
//     else
//     {
//       return "Let's fight again!";
//     }
//   }
// }