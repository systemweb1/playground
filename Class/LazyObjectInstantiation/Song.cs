using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace playground.Class.LazyObjectInstantiation
{
    public class Song
    {
        public string? Artist { get; set; }
        public string? TrackName { get; set; }
        public double TrackLength { get; set; }
    }
}