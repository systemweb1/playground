using playground.Interface;

namespace playground.Class.Shapes
{
    // The abstract base class of the hierarchy
    abstract class Shape
    {
        /// <summary>
        /// constructor default name = NoName
        /// </summary>
        /// <param name="name"></param>
        protected Shape(string name = "NoName")
        {
            PetName = name;
        }

        /// <summary>
        /// property
        /// </summary>
        public string PetName { get; }

        /// <summary>
        /// A single virtual method.
        /// </summary>
        public virtual void Draw1() => Console.WriteLine("Inside Shape.Draw()");

        /// <summary>
        /// Force all child classes to define how to be rendered.
        /// </summary>
        public abstract void Draw2();
    }
}