namespace playground.Class.Shapes
{
    //Circle.cs
    // Circle DOES NOT override Draw().
    class Circle : Shape
    {
        public Circle() { }
        public Circle(string name) : base(name) { } // base() = Shape()

        // If we did not implement the abstract Draw() method, Circle would also be
        // considered abstract, and would have to be marked abstract!
        public override void Draw2() => Console.WriteLine($"Drawing {PetName} the Circle");
    }
}