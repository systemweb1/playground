using playground.Interface;

namespace playground.Class.Shapes
{
    // Hexagon DOES override Draw().
    // can implement morethan one interface
    class Hexagon : Shape, IPointy, IDraw3D
    {
        public Hexagon() { }
        public Hexagon(string name) : base(name) { }

        public override void Draw2()
        {
            Console.WriteLine("Drawing {0} the Hexagon", PetName);
        }
        // IPointy implementation.
        public byte Points => 6;

        // public override void Draw1() => Console.WriteLine($"Drawing {PetName} the Hexagon");
        // public override void Draw2() => Console.WriteLine($"Drawing {PetName} the Hexagon");

        // Hexagon supports IPointy and IDraw3D.
        public void Draw3D() => Console.WriteLine("Drawing Hexagon in 3D!");
    }
}