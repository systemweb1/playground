using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace playground.Class.Shapes
{
    class Rectangle : IShape
    {
        public int GetNumberOfSides() => 4;
        public void Draw() => Console.WriteLine("Drawing...");
        public void Print() => Console.WriteLine("Printing...");
    }
}