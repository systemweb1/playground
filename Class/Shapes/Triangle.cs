using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using playground.Interface;

namespace playground.Class.Shapes
{
    class Triangle : Shape, IPointy
    {
        public Triangle() { }
        public Triangle(string name) : base(name) { }
        public override void Draw2()
        {
            Console.WriteLine("Drawing {0} the Triangle", PetName);
        }
        // IPointy implementation.
        //public byte Points
        //{
        // get { return 3; }
        //}
        public byte Points => 3;
    }
}