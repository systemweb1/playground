using playground.Interface;

namespace playground.Class.Shapes
{
    /// <summary>
    /// ThreeDCircle “is-a” Circle, so you derive from your existing Circle type
    /// <br/>Circle supports IDraw3D.
    /// </summary>
    class ThreeDCircle : Circle, IDraw3D
    {
        /// <summary>
        /// Hide the PetName property above me.
        /// </summary>
        public new string? PetName { get; set; } //ShapeClass: public string PetName { get; set; }

        /// <summary>
        /// Hide any Draw() implementation above me.
        /// </summary>
        public new void Draw2()  //ShapeClass:  public void Draw()
        {
            Console.WriteLine("Drawing a 3D Circle");
        }

        public void Draw3D() => Console.WriteLine("Drawing Circle in 3D!");
    }
}