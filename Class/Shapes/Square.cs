using playground.Class.Shapes;
using playground.Interface;

namespace playground.Class.Shapes
{
    // class Square : Shape, IRegularPointy
    // {
    //     public Square() { }
    //     public Square(string name) : base(name) { }
    //     //Draw comes from the Shape base class
    //     public override void Draw2()
    //     {
    //         Console.WriteLine("Drawing a square");
    //     }
    //     //This comes from the IPointy interface
    //     public byte Points => 4;
    //     //These come from the IRegularPointy interface
    //     public int SideLength { get; set; }
    //     public int NumberOfSides { get; set; }
    //     //Note that the Perimeter property is not implemented
    // }

    class Square : IShape
    {
        // Using explicit implementation to handle member name clash.
        void IPrintable.Draw()
        {
            // Draw to printer ...
        }
        void IDrawable2.Draw()
        {
            // Draw to screen ...
        }
        public void Print()
        {
            // Print ...
        }
        public int GetNumberOfSides() => 4;
    }
}