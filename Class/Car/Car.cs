using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace playground.Class.Car
{
    class Car
    {
        // Constant for maximum speed.
        public const int MaxSpeed = 100;
        // Car properties.
        public int CurrentSpeed { get; set; } = 0;
        public string PetName { get; set; } = "";
        // Is the car still operational?
        private bool _carIsDead;
        // A car has-a radio.
        private readonly Radio _theMusicBox = new Radio();
        // Constructors.
        public Car() { }
        public Car(string name, int speed)
        {
            CurrentSpeed = speed;
            PetName = name;
        }
        public void CrankTunes(bool state)
        {
            // Delegate request to inner object.
            _theMusicBox.TurnOn(state);
        }

        #region Example of using throw Exception
        // See if Car has overheated.
        // This time, throw an exception if the user speeds up beyond MaxSpeed.
        public void Accelerate(int delta)
        {
            if (delta < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(delta), "Speed must be greater than zero");
            }

            if (_carIsDead)
            {
                Console.WriteLine($"{PetName} is out of order...");
            }
            else
            {
                CurrentSpeed += delta;
                if (CurrentSpeed >= MaxSpeed)
                {
                    CurrentSpeed = 0;
                    _carIsDead = true;
                    // Use the "throw" keyword to raise an exception.
                    //ex throw new Exception
                    // throw new Exception($"{PetName} has overheated!");

                    // //ex throw Exception
                    // throw Exception();

                    // and return to the caller.                   
                    // throw new Exception($"{PetName} has overheated!")
                    // {
                    //     //ex throw HelpLink property
                    //     HelpLink = "http://www.CarsRUs.com",
                    //     //ex throw data property
                    //     Data = {
                    //         //Key: TimeStamp
                    //         //Value: The car exploded at {DateTime.Now}
                    //         {"TimeStamp",$"The car exploded at {DateTime.Now}"},
                    //         {"Cause","You have a lead foot."}
                    //     }
                    // };

                    throw new CarIsDeadException("You have a lead foot", DateTime.Now, $"{PetName} has overheated!")
                    {
                        HelpLink = "http://www.CarsRUs.com",
                    };
                }
            }
            Console.WriteLine($"=> CurrentSpeed = {CurrentSpeed}");
        }
        #endregion

        private Exception Exception()
        {
            throw new NotImplementedException();
        }

        public override string ToString()
        {
            return $"{PetName} is going {CurrentSpeed} MPH!!!";
        }
    }
}