using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace playground.Class
{
    struct EmpStructs
    {
        public int Id;
        public string Name;
        public string Address;

        // Constructor with a parameter
        public EmpStructs(int employeeId, string employeeName, string employeeAddress)
        {
            Id = employeeId;
            Name = employeeName;
            Address = employeeAddress;
        }

        public void GetEmp()
        {
            Console.WriteLine($"Employee: {Id}: {Name}, address: {Address}");
        }
    }
}