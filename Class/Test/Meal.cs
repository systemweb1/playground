using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using playground.Interface;

namespace playground.Class
{
    class Meal: ICalculateSomething
    {
        public double Pasta { get; set; }
        
        public double Calculator()
        {
            return Pasta * 2;
        }
    }
}