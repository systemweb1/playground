using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using playground.Interface;

namespace playground.Class
{
    class Beverages : ICalculateSomething
    {
        public double Coke { get; set; }
        public double Milk { get; set; }

        public double Calculator()
        {
            return Coke + Milk;
        }
    }
}