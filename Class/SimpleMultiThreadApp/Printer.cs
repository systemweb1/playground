using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace playground.Class.SimpleMultiThreadApp
{
    public class Printer
    {
        // Lock token.
        private object threadLock = new object();

        // public void PrintNumbers()
        // {
        //     // Display Thread info.
        //     Console.WriteLine($"-> {Thread.CurrentThread.Name} is executing PrintNumbers()");

        //     // Print out numbers.
        //     Console.Write("Your numbers: ");
        //     for (int i = 0; i < 10; i++)
        //     {
        //         Console.Write("{0}, ", i);
        //         Thread.Sleep(2000);
        //     }
        //     Console.WriteLine();
        // }

        // //lock keyword
        // public void PrintNumbers()
        // {
        //     // Use the private object lock token.
        //     lock (threadLock)
        //     {
        //         // Display Thread info.
        //         Console.WriteLine($"-> {Thread.CurrentThread.Name} is executing PrintNumbers()");
        //         // Print out numbers.
        //         Console.Write("Your numbers: ");
        //         for (int i = 0; i < 10; i++)
        //         {
        //             Random r = new Random();
        //             Thread.Sleep(1000 * r.Next(5));
        //             Console.Write($"{i}, ");
        //         }
        //         Console.WriteLine();
        //     }
        // }

        // System.Threading.Monitor
        public void PrintNumbers()
        {
            Monitor.Enter(threadLock);
            try
            {
                // Display Thread info.
                Console.WriteLine($"-> {Thread.CurrentThread.Name} is executing PrintNumbers()");
                
                // Print out numbers.
                Console.Write("Your numbers: ");
                for (int i = 0; i < 10; i++)
                {
                    Random r = new Random();
                    Thread.Sleep(1000 * r.Next(5));
                    Console.Write($"{i}, ");
                }
                Console.WriteLine();
            }
            finally
            {
                Monitor.Exit(threadLock);
            }
        }
    }
}