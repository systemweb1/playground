namespace playground.Class
{
    public struct STPoint<T>
    {
        // Generic state data.
        private T? _xPos;
        private T? _yPos;
        
        // Generic constructor.
        public STPoint(T xVal, T yVal)
        {
            _xPos = xVal;
            _yPos = yVal;
        }

        public void ResetPoint()
        {
            _xPos = default(T);
            _yPos = default(T);
        }

        // Generic properties.
        public T X
        {
            get => _xPos;
            set => _xPos = value;
        }
        public T Y
        {
            get => _yPos;
            set => _yPos = value;
        }
        public override string ToString() => $"[{_xPos}, {_yPos}]";
    }
}