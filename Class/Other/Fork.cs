using playground.Interface;

namespace playground.Class
{
    class Fork : IPointy
    {
        public byte Points => 4;
    }
}