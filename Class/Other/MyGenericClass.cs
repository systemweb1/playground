using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;
using playground.Class.Shapes;
using playground.Interface;

namespace playground.Class
{
    // MyGenericClass derives from object, while
    // contained items must have a default ctor.
    public class MyGenericClass1<T> where T : new()
    {
        // This method will swap any structure, but not classes.
        static void Swap<T>(ref T a, ref T b) where T : struct
        {

        }

    }

    // MyGenericClass derives from object, while
    // contained items must be a class implementing IDrawable
    // and must support a default ctor.
    public class MyGenericClass2<T> where T : class, IDrawable, new()
    {

    }

    public static class Calculator
    {
        //     public static T Add<T>(T a, T b)
        //     {
        //         dynamic dynamicA = a;
        //         dynamic dynamicB = b;
        //         return dynamicA + dynamicB;
        //     }

        public static T Plus<T>(T a, T b) where T : INumber<T>
        {
            return a + b;
        }

        public static T Minus<T>(T a, T b) where T : INumber<T>
        {
            return a - b;
        }

        public static T Multiply<T>(T a) where T : INumber<T>
        {
            return a * a;
        }

        public static T Divide<T>(T a, T b) where T : INumber<T>
        {
            return a / b;
        }

        public static T UseParam<T>(params T[] a) where T : INumber<T>
        {
            T sum = default(T);
            // Initialize sum to the default value of T
            // foreach (T item in a)
            // {
            //     sum += item;
            // }
            // T sum = a[0];
            Console.WriteLine(a.Length);
            for (int i = 0; i <= a.Length - 1; i++)
            {
                // sum += a[i];
                if (a[i] != null) // Check for null value
                {
                    sum += a[i];
                }
                else
                {
                    throw new ArgumentNullException($"Element at index {i} is null");
                }
            }

            return sum;
        }
    }

    // // Error! new() constraint must be listed last!
    // public class MyGenericClass3<T> where T : new(), class, IDrawable
    // {

    // }

    // // <K> must extend SomeBaseClass and have a default ctor,
    // // while <T> must be a structure and implement the
    // // generic IComparable interface.
    // public class MyGenericClass<K, T> where K : SomeBaseClass, new()
    //  where T : struct, System.IComparable<T>
    // {

    // }
}