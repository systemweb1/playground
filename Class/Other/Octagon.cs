using playground.Interface;

namespace playground.Class
{
    class Octagon : IDrawToForm, IDrawToMemory, IDrawToPrinter
    {
        // Explicitly bind Draw() implementations to a given interface.
        void IDrawToForm.Draw()
        {
            Console.WriteLine("Drawing to form...");
        }
        void IDrawToMemory.Draw()
        {
            Console.WriteLine("Drawing to memory...");
        }
        void IDrawToPrinter.Draw()
        {
            Console.WriteLine("Drawing to a printer...");
        }
    }

    public class BitmapImage : IAdvancedDraw
    {
        public void Draw()
        {
            Console.WriteLine("Drawing...");
        }
        public void DrawInBoundingBox(int top, int left, int bottom, int right)
        {
            Console.WriteLine("Drawing in a box...");
        }
        public void DrawUpsideDown()
        {
            Console.WriteLine("Drawing upside down!");
        }
        public int TimeToDraw() => 12;
    }

    #region Interface: multiple inheritance with interface types
    //IDrawable.cs
    // Multiple inheritance for interface types is A-okay.
    interface IDrawable2
    {
        void Draw();
    }
    //IPrintable.cs
    interface IPrintable
    {
        void Print();
        void Draw(); // <-- Note possible name clash here!
    }
    //IShape.cs
    // Multiple interface inheritance. OK!
    interface IShape : IDrawable2, IPrintable
    {
        int GetNumberOfSides();
    }
    #endregion
}