using playground.Interface;

namespace playground.Class
{
    class Knife : IPointy
    {
        public byte Points => 1;
    }
}