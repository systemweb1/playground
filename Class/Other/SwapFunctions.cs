using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using playground.Program;

namespace playground.Class
{
    public class SwapFunctions
    {
        // Swap two integers.
        static void Swap(ref int a, ref int b)
        {
            int temp = a;
            a = b;
            b = temp;
        }

        #region Swap
        // Swap two Person objects.
        static void Swap(ref Person a, ref Person b)
        {
            Person temp = a;
            a = b;
            b = temp;
        }
        #endregion

        #region Swap<T>
        // This method will swap any two items.
        // as specified by the type parameter <T>.
        internal static void Swap<T>(ref T a, ref T b)
        {
            Console.WriteLine($"You sent the Swap() method a {typeof(T)}");
            T temp = a;
            a = b;
            b = temp;
        }
        #endregion
    }
}