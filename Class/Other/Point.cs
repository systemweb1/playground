using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace playground.Class
{
    public class Point : ICloneable
    {

        public int X { get; set; }
        public int Y { get; set; }
        public PointDescription desc = new PointDescription();
        public Point(int xPos, int yPos, string petName)
        {
            X = xPos; Y = yPos;
            desc.PetName = petName;
        }
        public Point(int xPos, int yPos)
        {
            X = xPos; Y = yPos;
        }
        public Point() { }

        #region overloaded
        // Add 1 to the X/Y values for the incoming Point.
        public static Point operator ++(Point p1)
        => new Point(p1.X + 1, p1.Y + 1);
        #endregion

        // Override Object.ToString().
        public override string ToString()
        => $"X = {X}; Y = {Y}; Name = {desc.PetName};\nID = {desc.PointID}\n";

        public object Clone()
        {
            // First get a shallow copy.
            //not change pointID
            Point newPoint = (Point)this.MemberwiseClone();

            // Then fill in the gaps.
            //change pointID
            PointDescription currentDesc = new PointDescription();
            currentDesc.PetName = this.desc.PetName;
            newPoint.desc = currentDesc;
            return newPoint;
        }
    }

    // This class describes a point.
    public class PointDescription
    {
        public string PetName { get; set; }
        public Guid PointID { get; set; }
        public PointDescription()
        {
            PetName = "No-name";
            PointID = Guid.NewGuid();
        }
    }
}