using playground.Interface;

namespace playground.Class
{
    class PitchFork : IPointy
    {
        public byte Points => 3;
    }
}