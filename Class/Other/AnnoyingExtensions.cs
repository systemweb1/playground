using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace playground.Class
{
    static class AnnoyingExtensions
    {
        //first parameters of method must be use 'this' keyword
        public static void PrintDataAndBeep(this System.Collections.IEnumerable iterator)
        {
            foreach (var item in iterator)
            {
                Console.WriteLine(item);
                Console.Beep();
            }
        }
    }
}