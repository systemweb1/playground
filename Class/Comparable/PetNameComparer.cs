using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace playground.Class.Comparable
{
    // This helper class is used to sort an array of Cars by pet name.
    public class PetNameComparer : IComparer
    {
        // Test the pet name of each object.
        int IComparer.Compare(object? o1, object? o2)
        {
            if (o1 is ComparableCar t1 && o2 is ComparableCar t2)
            {
                return string.Compare(t1.ComPetName, t2.ComPetName,
                StringComparison.OrdinalIgnoreCase);
            }
            else
            {
                throw new ArgumentException("Parameter is not a Car!");
            }
        }
    }
}