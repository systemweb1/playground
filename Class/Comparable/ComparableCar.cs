using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using playground.Class.Car;

namespace playground.Class.Comparable
{
    // The iteration of the Car can be ordered
    // based on the CarID.
    public class ComparableCar : IComparable<ComparableCar>
    {
        public int CarID { get; set; }
        public int ComCurrentSpeed { get; set; } = 0;
        public string ComPetName { get; set; } = "";

        public ComparableCar(string name, int currSp, int id)
        {
            ComCurrentSpeed = currSp;
            ComPetName = name;
            CarID = id;
        }

        // Property to return the PetNameComparer.
        public static IComparer SortByPetName
        => (IComparer)new PetNameComparer();

        public static IComparer SortByCarID 
        => (IComparer)new PetNameComparer();

        // public int CompareTo(object? obj)
        // {
        //     throw new NotImplementedException();
        // }

        // // IComparable implementation.
        // int IComparable.CompareTo(object obj)
        // {
        //     if (obj is ComparableCar temp)
        //     {
        //         //  Compare return value
        //         //         if (this.CarID > temp.CarID)
        //         //         {
        //         //             return 1;
        //         //         }
        //         //         if (this.CarID < temp.CarID)
        //         //         {
        //         //             return -1;
        //         //         }
        //         //         return 0;

        //         return this.CarID.CompareTo(temp.CarID);
        //     }
        //     throw new ArgumentException("Parameter is not a Car!");
        // }

        public int CompareTo(ComparableCar? obj)
        {
            if (this.CarID > obj?.CarID)
            {
                return 1;
            }
            if (this.CarID < obj?.CarID)
            {
                return -1;
            }
            return 0;

            // return newOBJ;
        }
        
        public override string ToString()
        {
            return $"{CarID}: {ComPetName} {ComCurrentSpeed}";
        }
    }
}