using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace playground.Class
{
    public class Complex
    {
        public int Real { get; set; }
        public int Imaginary { get; set; }

        public Complex(int r, int i)
        {
            Real = r;
            Imaginary = i;
        }
    }
}