using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace playground.Class.LambdaExpression
{
    public class SimpleMath
    {
        public delegate void MathMessage(string msg, int result);
        private MathMessage? _mmDelegate;

        public void SetMathHandler(MathMessage target)
        {
            _mmDelegate = target;
        }

        // //not use lambda expression
        // public void Add(int x, int y)
        // {
        //     _mmDelegate?.Invoke("Adding has completed!", x + y);
        // }

        //use lambda expression
        public int Add(int x, int y) => x + y;
        public void PrintSum(int x, int y) => Console.WriteLine(x + y);
    }
}