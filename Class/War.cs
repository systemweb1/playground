using System.Collections.Generic; // Add this line to import the Dictionary class

public class Kata
{
    public static string AlphabetWar(string fight)
    {
        var LeftSidePower = new Dictionary<char, int> {
      {'w', 4},
      {'p', 3},
      {'b', 2},
      {'s', 1},
      {'t', 0} // Priest with Wo lo loooooooo power
    };

        var RightSidePower = new Dictionary<char, int> {
      {'m', 4},
      {'q', 3},
      {'d', 2},
      {'z', 1},
      {'j', 0} // Priest with Wo lo loooooooo power
    };

        //total power for both side
        int LeftTotalPower = 0;
        int RightTotalPower = 0;
        bool ChangedNext = false;

        for (int i = 0; i < fight.Length; i++)
        {
            char CurrentLetter = fight[i];

            if (CurrentLetter == 't' || CurrentLetter == 'j')
            {
                if (i > 0 && fight[i - 1] == CurrentLetter)
                {
                    // Adjacent letter is also a priest, no conversion
                    continue;
                }
                else if (i < fight.Length - 1 && fight[i + 1] == CurrentLetter)
                {
                    // Adjacent letter is also a priest, no conversion
                    continue;
                }
                else if (LeftSidePower.ContainsKey(CurrentLetter))
                {
                    //   if (RightSidePower.ContainsKey(fight[i + 1]))
                    //   {
                    //     LeftTotalPower += RightSidePower[fight[i + 1]];
                    //     ChangedNext = true;
                    //   }

                    //   if (ChangedNext)
                    //   {
                    //     i++;
                    //   }



                    if (i == fight.Length - 1 || (i < fight.Length - 1 && fight[i + 1] != 'j') || (fight[i + 1] == 'j' && (i > 0 && fight[i - 1] == 't')))
                    {
                        if (fight[i - 1] != 'j')
                        {
                            LeftTotalPower += LeftSidePower[CurrentLetter];
                            ChangedNext = true;
                        }
                        else
                        {
                            RightTotalPower += LeftSidePower[CurrentLetter];
                            ChangedNext = true;
                        }
                    }

                    if (ChangedNext)
                    {
                        i++;
                    }

                }
                else if (RightSidePower.ContainsKey(CurrentLetter))
                {
                    // if (LeftSidePower.ContainsKey(fight[i + 1]))
                    // {
                    //     RightTotalPower += LeftSidePower[fight[i + 1]];
                    //     ChangedNext = true;
                    // }

                    // if (ChangedNext)
                    // {
                    //     i++;
                    // }



                    if (i == fight.Length - 1 || (i < fight.Length - 1 && fight[i + 1] != 't') || (fight[i + 1] == 't' && (i > 0 && fight[i - 1] == 'j')))
                    {
                        if (fight[i - 1] != 't')
                        {
                            RightTotalPower += RightSidePower[CurrentLetter];
                            ChangedNext = true;
                        }
                        else
                        {
                            LeftTotalPower += RightSidePower[CurrentLetter];
                            ChangedNext = true;
                        }

                        if (ChangedNext)
                        {
                            i++;
                        }
                    }
                }
            }
            else if (LeftSidePower.ContainsKey(CurrentLetter))
            {
                if (i == fight.Length - 1 || (i < fight.Length - 1 && fight[i + 1] != 'j') || (fight[i + 1] == 'j' && (i > 0 && fight[i - 1] == 't')))
                {
                    LeftTotalPower += LeftSidePower[CurrentLetter];
                }
            }

            else if (RightSidePower.ContainsKey(CurrentLetter))
            {
                if (i == fight.Length - 1 || (i < fight.Length - 1 && fight[i + 1] != 't') || (fight[i + 1] == 't' && (i > 0 && fight[i - 1] == 'j')))
                {
                    RightTotalPower += RightSidePower[CurrentLetter];
                }
            }

            // Calculate power for the current letter
            if (LeftSidePower.ContainsKey(CurrentLetter))
            {
                LeftTotalPower += LeftSidePower[CurrentLetter];
            }
            else if (RightSidePower.ContainsKey(CurrentLetter))
            {
                RightTotalPower += RightSidePower[CurrentLetter];
            }
        }

        // Determine the winner
        if (LeftTotalPower > RightTotalPower)
        {
            return "Left side wins!";
        }
        else if (RightTotalPower > LeftTotalPower)
        {
            return "Right side wins!";
        }
        else
        {
            return "Let's fight again!";
        }
    }
}