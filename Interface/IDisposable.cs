using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace playground.Interface
{
    public interface IDisposable
    {
        void Dispose();
    }
}