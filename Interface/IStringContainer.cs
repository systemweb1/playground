namespace playground.Interface
{
    public interface IStringContainer
    {
        string this[int index] { get; set; }
    }
}