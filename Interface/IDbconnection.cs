namespace playground.Interface
{
    // abstract class Shape
    // {
    //     // Every derived class must now support this method!
    //     public abstract byte GetNumberOfPoints();
    // }

    // This interface defines the behavior of "having points."
    // The pointy behavior as a read-only property.
    public interface IPointy
    {
        // Implicitly public and abstract.
        //byte GetNumberOfPoints();
        // A read-write property in an interface would look like:
        //string PropName { get; set; }
        // while a read-only property in an interface would be:
        byte Points { get; }
    }
}