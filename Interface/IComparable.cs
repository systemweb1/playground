using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace playground.Interface
{
    public interface IComparable
    {
        int CompareTo(object o);
    }
}