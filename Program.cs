﻿using System.Collections;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Data;
using System.Diagnostics;
using System.Reflection;
using System.Text;
using playground.Class;
using playground.Class.Car;
using playground.Class.Comparable;
using playground.Class.Events;
using playground.Class.LambdaExpression;
using playground.Class.LazyObjectInstantiation;
using playground.Class.LINQ;
using playground.Class.OverloadOperator;
using playground.Class.Shapes;
using playground.Class.SimpleMultiThreadApp;
using playground.Interface;

namespace playground.Program
{
    class Program
    {
        static void Main(string[] args)
        {
            #region
            // Employee emp = new Employee("I'm Employee.", 30, "John");
            // emp.SetEmpName("Mathew");
            // Console.WriteLine(emp);
            // Console.ReadLine();
            #endregion

            #region polymorphism interface: single virtual method
            // Console.WriteLine("***** Fun with Polymorphism *****\n");
            // Hexagon hex = new Hexagon("Beth");
            // hex.Draw1();
            // Circle cir = new Circle("Cindy");
            // // Calls base class implementation!
            // cir.Draw1();
            // Console.ReadLine();
            #endregion

            #region polymorphism interface: all child classes to define how to be rendered
            // Console.WriteLine("***** Fun with Polymorphism *****\n");
            // // Make a list of Shape-compatible objects.
            // List<Shape> myShapes = new List<Shape>() {
            //     new Hexagon(),
            //     new Circle(),
            //     new Hexagon("Mick"),
            //     new Circle("Beth"),
            //     new Hexagon("Linda")
            // };
            // // Loop over each item and interact with the
            // // polymorphic interface.
            // myShapes.ForEach(s =>
            // {
            //     s.Draw2();
            // });
            // Console.ReadLine();
            #endregion

            #region polymorphism: Member Shadowing
            // // This calls the Draw() method of the ThreeDCircle.
            // Console.WriteLine("***** This calls the Draw() method of the ThreeDCircle *****\n");
            // ThreeDCircle o = new ThreeDCircle();
            // o.Draw();
            // Console.ReadLine();

            // // This calls the Draw() method of the parent!
            // Console.WriteLine("***** This calls the Draw() method of the parent! *****\n");
            // ((Circle)o).Draw2();//explicit cast: (ClassIWantToCastTo)referenceIHave
            // Console.ReadLine();
            #endregion

            #region
            // Person a = new Person("A", 20);
            // Person b = new Person("A", 20);
            // Console.WriteLine($"is a equal b: {a.Equals(b)}");
            // Console.WriteLine($"a.GetHashCode(): {a.GetHashCode()}");
            // Console.WriteLine($"b.GetHashCode(): {b.GetHashCode()}");
            // Console.WriteLine($"a.GetType(): {a.GetType()}");

            // Person c = new Person("B", 20);
            // Person d = c;
            // object obj = d;
            // Console.WriteLine($"c.Equals(d): {c.Equals(d)}");

            // Console.WriteLine($"o.Equals(c): {obj.Equals(c)}");
            // Console.WriteLine($"d.GetHashCode(): {d.GetHashCode()}");
            // Console.WriteLine($"c.GetHashCode(): {c.GetHashCode()}");
            // Console.WriteLine($"o.GetHashCode(): {obj.GetHashCode()}");
            // Console.WriteLine($"o.GetType(): {obj.GetType()}");

            // Console.WriteLine($"'a' and 'b'' have same state: {object.Equals(a, b)}"); //can override
            // Console.WriteLine($"'c' and 'd' are pointing to same object: {object.ReferenceEquals(c, d)}");//
            #endregion

            #region Exceptions
            // Console.WriteLine("***** Simple Exception Example *****");
            // Console.WriteLine("=> Creating a car and stepping on it!");
            // Car myCar = new Car("Zippy", 20);
            // myCar.CrankTunes(true);
            // //use try and catch
            // try
            // {
            //     for (int i = 0; i < 10; i++)
            //     {
            //         myCar.Accelerate(10);
            //     }
            // }
            // catch (Exception e)
            // {
            //     // Handle the thrown exception.
            //     Console.WriteLine("\n*** Error! ***");
            //     Console.WriteLine($"Method: {e.TargetSite}");
            //     Console.WriteLine($"Method - DeclaringType: {e.TargetSite?.DeclaringType}");
            //     Console.WriteLine($"Method - MemberType: {e.TargetSite?.MemberType}");
            //     Console.WriteLine($"Message: {e.Message}");
            //     Console.WriteLine($"Source: {e.Source}");
            //     Console.WriteLine($"StackTrace: {e.StackTrace}");
            //     Console.WriteLine($"InnerException: {e.InnerException}");
            //     Console.WriteLine($"ToString: {e.ToString}");
            //     Console.WriteLine("Help Link: {0}", e.HelpLink);//Example of HeapLink Exception

            //     #region ex for data property
            //     Console.WriteLine("\n-> Custom Data:");
            //     foreach (DictionaryEntry de in e.Data)
            //     {
            //         Console.WriteLine($"-> {de.Key}: {de.Value}");
            //     }
            //     #endregion
            // }
            // Console.WriteLine("\n***** Out of exception logic *****");
            // Console.ReadLine();
            #endregion

            #region Exception: multiple exception
            // // This code will not compile!
            // Console.WriteLine("***** Handling Multiple Exceptions *****\n");
            // Car myCar = new Car("Rusty", 90);
            // try
            // {
            //     // Trigger an argument out of range exception.
            //     myCar.Accelerate(-10);
            // }
            // catch (CarIsDeadException e)
            // {
            //     Console.WriteLine(e.Message);
            // }
            // catch (ArgumentOutOfRangeException e)
            // {
            //     Console.WriteLine(e.Message);
            // }
            // // This is handle anything derive from System.Exception(is-a relationship). 
            // // If it's define before CarIsDeadException and ArgumentOutOfRangeException, 
            // // the final two catch blocks are unreachable!
            // catch (Exception e)
            // {
            //     // Process all other exceptions?
            //     Console.WriteLine(e.Message);
            // }
            // Console.ReadLine();
            #endregion

            #region Exception: general cath exception
            // // A generic catch.
            // Console.WriteLine("***** Handling Multiple Exceptions *****\n");
            // Car myCar = new Car("Rusty", 90);
            // try
            // {
            //     myCar.Accelerate(90);
            // }
            // catch
            // {
            //     Console.WriteLine("Something bad happened...");
            // }
            // Console.ReadLine();
            #endregion

            #region Exception: rethrowing exception
            // // A generic catch.
            // Console.WriteLine("***** Handling Multiple Exceptions *****\n");
            // Car myCar = new Car("Rusty", 90);
            // try
            // {
            //     myCar.Accelerate(90);
            // }
            // catch (CarIsDeadException e)
            // {
            //     // Do any partial processing of this error and pass the buck.
            //     throw;
            // }
            // Console.ReadLine();
            #endregion

            #region Exception: inner exception
            // // A generic catch.
            // Console.WriteLine("***** Handling Multiple Exceptions *****\n");
            // Car myCar = new Car("Rusty", 90);
            // try
            // {
            //     myCar.Accelerate(90);
            // }
            // //Update the exception handler
            // catch (CarIsDeadException e)
            // {
            //     try
            //     {
            //         FileStream fs = File.Open(@"C:\carErrors.txt", FileMode.Open);
            //     }
            //     catch (Exception e2)
            //     {
            //         //This causes a compile error-InnerException is read only
            //         //e.InnerException = e2;
            //         // Throw an exception that records the new exception,
            //         // as well as the message of the first exception.
            //         throw new CarIsDeadException(e.CauseOfError, e.ErrorTimeStamp, e.Message, e2);
            //     }
            // }

            // Console.ReadLine();
            #endregion

            #region Exception: finally box
            // Console.WriteLine("***** Handling Multiple Exceptions *****\n");
            // Car myCar = new Car("Rusty", 90);
            // myCar.CrankTunes(true);
            // try
            // {
            //     // Speed up car logic.
            // }
            // catch (CarIsDeadException e)
            // {
            //     // Process CarIsDeadException.
            // }
            // catch (ArgumentOutOfRangeException e)
            // {
            //     // Process ArgumentOutOfRangeException.
            // }
            // catch (Exception e)
            // {
            //     // Process any other Exception.
            // }
            // finally
            // {
            //     // This will always occur. Exception or not.
            //     myCar.CrankTunes(false);
            // }
            // Console.ReadLine();
            #endregion

            #region Interfaced
            // Console.WriteLine("***** Fun with Interfaces *****\n");
            // // Call Points property defined by IPointy.
            // Hexagon hex = new Hexagon();
            // Console.WriteLine($"Points: {hex.Points}");
            // Console.ReadLine();

            // //use 'as' keyword
            // // Can we treat hex2 as IPointy?
            // Hexagon hex2 = new Hexagon("Peter");
            // IPointy itfPt2 = hex2 as IPointy;
            // if (itfPt2 != null)
            // {
            //     Console.WriteLine($"Points: {itfPt2.Points}");
            // }
            // else
            // {
            //     Console.WriteLine("OOPS! Not pointy...");
            // }
            // Console.ReadLine();

            // //use 'is' keyword
            // Console.WriteLine("***** Fun with Interfaces *****\n");
            // if (hex2 is IPointy itfPt3)
            // {
            //     Console.WriteLine("Points: {0}", itfPt3.Points);
            // }
            // else
            // {
            //     Console.WriteLine("OOPS! Not pointy...");
            // }
            // Console.ReadLine();
            #endregion

            #region Interface: implementation
            // var sq = new Square("Boxy")
            // { NumberOfSides = 4, SideLength = 4 };
            // sq.Draw2();
            // //This won't compile
            // //Console.WriteLine($"{sq.PetName} has {sq.NumberOfSides} of length {sq.SideLength} and a 
            // // perimeter of { sq.Perimeter}
            // // ");
            #endregion

            #region Interface: interface as parameters
            // Shape[] myShapes = { new Hexagon(), new Circle(), new Triangle("Joe"), new Circle("JoJo") };
            // for (int i = 0; i < myShapes.Length; i++)
            // {
            //     // Can I draw you in 3D?
            //     if (myShapes[i] is IDraw3D s)
            //     {
            //         DrawIn3D(s);
            //     }
            // }
            #endregion

            #region Interface: array of interface types
            // // This array can only contain types that implement the IPointy interface.
            // IPointy[] myPointyObjects = { new Hexagon(), new Knife(), new Triangle(), new Fork(), new PitchFork() };
            // foreach (IPointy i in myPointyObjects)
            // {
            //     Console.WriteLine($"Object has {i.Points} points.");
            // }
            // Console.ReadLine();
            #endregion

            #region Interface: explicit interface implementation
            // Console.WriteLine("***** Fun with Interface Name Clashes *****\n");
            // Octagon oct = new Octagon();
            // // Both of these invocations call the
            // // same Draw() method!
            // // Shorthand notation if you don't need
            // // the interface variable for later use.
            // ((IDrawToPrinter)oct).Draw();

            // // Could also use the "is" keyword.
            // if (oct is IDrawToMemory dtm)
            // {
            //     dtm.Draw();
            // }
            // Console.ReadLine();
            #endregion

            #region Interface: Interface Hierarchies
            // // Console.WriteLine("***** Simple Interface Hierarchy *****");
            // // // Call from object level.
            // BitmapImage myBitmap = new BitmapImage();
            // myBitmap.Draw();
            // myBitmap.DrawInBoundingBox(10, 10, 100, 150);
            // myBitmap.DrawUpsideDown();

            // if (myBitmap is IAdvancedDraw iAdvDraw)
            // {
            //     iAdvDraw.DrawUpsideDown();
            //     Console.WriteLine($"Time to draw: {iAdvDraw.TimeToDraw()}");
            // }

            // //Always calls method on instance:
            // Console.WriteLine("***** Calling Implemented TimeToDraw *****");
            // Console.WriteLine($"Time to draw: {myBitmap.TimeToDraw()}");
            // Console.WriteLine($"Time to draw: {((IDrawable)myBitmap).TimeToDraw()}");
            // Console.WriteLine($"Time to draw: {((IAdvancedDraw)myBitmap).TimeToDraw()}");
            #endregion

            #region Interface: ICloneable
            // Console.WriteLine("***** Fun with Object Cloning *****\n");
            // Console.WriteLine("Cloned p3 and stored new Point in p4");
            // Point p3 = new Point(100, 100, "Jane");
            // Point p4 = (Point)p3.Clone();
            // Console.WriteLine("Before modification:");
            // Console.WriteLine($"p3: {p3}");
            // Console.WriteLine($"p4: {p4}");
            // p4.desc.PetName = "My new Point";
            // p4.X = 9;
            // Console.WriteLine("\nChanged p4.desc.petName and p4.X");
            // Console.WriteLine("After modification:");
            // Console.WriteLine($"p3: {p3}");
            // Console.WriteLine($"p4: {p4}");
            // Console.ReadLine();
            #endregion

            #region Comparable and Comparer
            // Console.WriteLine("***** Fun with Object Sorting *****\n");
            // // Make an array of Car objects.
            // ComparableCar[] myAutos = new ComparableCar[5];
            // myAutos[0] = new ComparableCar("Rusty", 80, 1);
            // myAutos[1] = new ComparableCar("Mary", 40, 234);
            // myAutos[2] = new ComparableCar("Viper", 40, 34);
            // myAutos[3] = new ComparableCar("Mel", 40, 4);
            // myAutos[4] = new ComparableCar("Chucky", 40, 5);

            // // Exercise the IComparable interface.
            // // Make an array of Car objects.
            // // Display current array.
            // Console.WriteLine("Here is the unordered set of cars:");
            // foreach (ComparableCar c in myAutos)
            // {
            //     Console.WriteLine($"{c.CarID} {c.ComPetName}");
            // }
            // // Now, sort them using IComparable!
            // Array.Sort(myAutos);
            // Console.WriteLine();
            // // Display sorted array.
            // Console.WriteLine("Here is the ordered set of cars:");
            // foreach (ComparableCar c in myAutos)
            // {
            //     Console.WriteLine($"{c.CarID} {c.ComPetName}");
            // }
            // Console.ReadLine();

            // // Now sort by pet name.
            // Array.Sort(myAutos, new PetNameComparer());

            // // Sorting by pet name made a bit cleaner.
            // Array.Sort(myAutos, ComparableCar.SortByPetName);

            // // Dump sorted array.
            // Console.WriteLine("Ordering by pet name:");
            // foreach (ComparableCar c in myAutos)
            // {
            //     Console.WriteLine($"{c.ComPetName} {c.CarID}");
            // }
            #endregion

            #region Gabage Collection
            // Console.WriteLine("***** Fun with System.GC *****");

            // // Print out estimated number of bytes on heap.
            // Console.WriteLine($"Estimated bytes on heap: {GC.GetTotalMemory(false)}");

            // // MaxGeneration is zero based.
            // Console.WriteLine($"This OS has {GC.MaxGeneration + 1} object generations.\n");

            // Car refToMyCar = new Car("Zippy", 100);
            // Console.WriteLine(refToMyCar.ToString());

            // // Print out generation of refToMyCar.
            // Console.WriteLine($"\nGeneration of refToMyCar is: {GC.GetGeneration(refToMyCar)}");

            // // Make a ton of objects for testing purposes.
            // object[] tonsOfObjects = new object[50000];
            // for (int i = 0; i < 50000; i++)
            // {
            //     tonsOfObjects[i] = new object();
            // }

            // // Collect only gen 0 objects.
            // Console.WriteLine("Force Garbage Collection");
            // GC.Collect(0, GCCollectionMode.Forced);
            // GC.WaitForPendingFinalizers();

            // // Print out generation of refToMyCar.
            // Console.WriteLine($"Generation of refToMyCar is: {GC.GetGeneration(refToMyCar)}");

            // // See if tonsOfObjects[9000] is still alive.
            // if (tonsOfObjects[9000] != null)
            // {
            //     Console.WriteLine($"Generation of tonsOfObjects[9000] is: {GC.GetGeneration(tonsOfObjects[9000])}");
            // }
            // else
            // {
            //     Console.WriteLine("tonsOfObjects[9000] is no longer alive.");
            // }

            // // Print out how many times a generation has been swept.
            // Console.WriteLine($"\nGen 0 has been swept {GC.CollectionCount(0)} times");
            // Console.WriteLine($"Gen 1 has been swept {GC.CollectionCount(1)} times");
            // Console.WriteLine($"Gen 2 has been swept {GC.CollectionCount(2)} times");
            // Console.ReadLine();
            // #endregion

            // #region Finalizers
            // Console.WriteLine("***** Fun with Finalizers *****\n");
            // Console.WriteLine("Hit return to create the objects ");
            // Console.WriteLine("then force the GC to invoke Finalize()");

            // //Depending on the power of your system,
            // //you might need to increase these values
            // // CreateObjects(1_000_000);
            // CreateObjects(5);

            // //Artificially inflate the memory pressure
            // GC.AddMemoryPressure(2147483647);
            // GC.Collect(0, GCCollectionMode.Forced);
            // GC.WaitForPendingFinalizers();
            // Console.ReadLine();
            #endregion

            #region Dispose
            // Console.WriteLine("***** Fun with Dispose *****\n");
            // // Create a disposable object and call Dispose()
            // // to free any internal resources.
            // // Use a comma-delimited list to declare multiple objects to dispose.
            // using (MyResourceWrapper rw = new MyResourceWrapper(), rw2 = new MyResourceWrapper())
            // {
            //     // Use rw and rw2 objects.
            //     try
            //     {
            //         // Use the members of rw.
            //         if (rw is System.IDisposable)
            //         {
            //             rw.Dispose(); //return rw.Dispose(); #1
            //         }
            //     }
            //     finally
            //     {
            //         // Always call Dispose(), error or not.
            //         rw2.Dispose(); //return rw.Dispose(); #2
            //     }
            //     //return rw.Dispose(); #3, rw2.Dispose() #4 -> cuz using rw and rw2 so it return twice!
            // }
            #endregion

            #region Declaration
            // Console.WriteLine("***** Dispose() / Destructor Combo Platter *****");

            // // Call Dispose() manually. This will not call the finalizer.
            // MyResourceWrapper rw = new MyResourceWrapper();
            // rw.Dispose();

            // // Don't call Dispose(). This will trigger the finalizer when the object gets GCd.
            // MyResourceWrapper rw2 = new MyResourceWrapper();
            #endregion

            #region LazyObjectInstantiation
            // Console.WriteLine("***** Fun with Lazy Instantiation *****\n");

            // // No allocation of AllTracks object here!
            // MediaPlayer myPlayer = new MediaPlayer();
            // myPlayer.Play();

            // // Allocation of AllTracks happens when you call GetAllTracks().
            // MediaPlayer yourPlayer = new MediaPlayer();
            // AllTracks yourMusic = yourPlayer.GetAllTracks();

            // Console.ReadLine();
            #endregion

            #region box and unbox
            // int i = 20;
            // object boxedInt = i; //Box the int into an object reference

            // int unboxedInt = (int)boxedInt; // Unbox the reference back into a corresponding int
            #endregion

            #region test list
            // // Console.WriteLine("---- b4 ----");


            // // var stopwatch = new Stopwatch();
            // // stopwatch.Start();

            // // var number = Enumerable.Range(0, 2_000_000_001);
            // // var list = new List<int>();
            // // list.AddRange(number);

            // // stopwatch.Stop();
            // // PrintMemoryUsage();
            // // Console.WriteLine(stopwatch.ElapsedMilliseconds + "ms");


            // // Console.WriteLine("---- After ----");
            // // // GC.Collect(); // Force garbage collection
            // // // PrintMemoryUsage();

            // // stopwatch.Restart();

            // // var numberAT = Enumerable.Range(0, 1_000_000_001);
            // // List<int> listAT = new List<int>(numberAT);
            // // // listAT.AddRange(numberAT);

            // // var n = listAT.FirstOrDefault(f => f == 970_000);
            // // Console.WriteLine(n);

            // // stopwatch.Stop();
            // // Console.WriteLine(stopwatch.ElapsedMilliseconds + "ms");
            // // PrintMemoryUsage();

            // // int a = 32;
            // // int b = 5;
            // // object s = a;
            // // var aaa = b.CompareTo(s);
            // // List<int> number = new List<int>(4) { 4, 3, 1, 2, 7 };
            // // number.Sort();
            // // number.ForEach(f =>
            // // {
            // //     Console.WriteLine(f);
            // // });

            // ComparableCar[] myAutos = new ComparableCar[5];
            // myAutos[0] = new ComparableCar("Rusty", 80, 1);
            // myAutos[1] = new ComparableCar("Mary", 40, 234);
            // myAutos[2] = new ComparableCar("Viper", 40, 34);
            // myAutos[3] = new ComparableCar("Mel", 40, 4);
            // myAutos[4] = new ComparableCar("Chucky", 40, 5);
            // var lst = new List<ComparableCar>(myAutos);
            // var a = lst.OrderByDescending(o => o.CarID).ToList();

            // a.ForEach(f =>
            // {
            //     Console.WriteLine(f.ToString());
            // });
            #endregion

            #region ObservableCollection<T>
            // // Make a collection to observe
            // //and add a few Person objects.
            // ObservableCollection<Person> people = new ObservableCollection<Person>()
            // {
            //     new Person{ FirstName = "Peter", LastName = "Murphy", Age = 52 },
            //     new Person{ FirstName = "Kevin", LastName = "Key", Age = 48 },
            // };
            // // Wire up the CollectionChanged event.
            // people.CollectionChanged += people_CollectionChanged;
            // // Now add a new item.
            // people.Add(new Person("Fred", "Smith", 32));
            // // Remove an item.
            // people.RemoveAt(0);
            #endregion

            #region Swap
            // Console.WriteLine("***** Fun with Custom Generic Methods *****\n");
            // // Swap 2 ints.
            // int a = 10, b = 90;
            // Console.WriteLine($"Before swap: {a}, {b}");
            // SwapFunctions.Swap<int>(ref a, ref b);
            // Console.WriteLine($"After swap: {a}, {b}");
            // Console.WriteLine();
            // // Swap 2 strings.
            // string s1 = "Hello", s2 = "There";
            // Console.WriteLine($"Before swap: {s1} {s2}!");
            // SwapFunctions.Swap<string>(ref s1, ref s2);
            // Console.WriteLine($"After swap: {s1} {s2}!");
            // Console.ReadLine();
            #endregion

            #region 
            // Console.WriteLine("***** Fun with Generic Structures *****\n");
            // // Point using ints.
            // STPoint<int> p = new STPoint<int>(10, 10);
            // Console.WriteLine($"p.ToString()={p.ToString()}");
            // p.ResetPoint();
            // Console.WriteLine($"p.ToString()={p.ToString()}");
            // Console.WriteLine();
            // // Point using double.
            // STPoint<double> p2 = new STPoint<double>(5.4, 3.3);
            // Console.WriteLine($"p2.ToString()={p2.ToString()}");
            // p2.ResetPoint();
            // Console.WriteLine($"p2.ToString()={p2.ToString()}");
            // Console.WriteLine();
            // // Point using strings.
            // STPoint<string> p3 = new STPoint<string>("i", "3i");
            // Console.WriteLine($"p3.ToString()={p3.ToString()}");
            // p3.ResetPoint();
            // Console.WriteLine($"p3.ToString()={p3.ToString()}");

            // STPoint<string> p4 = default;
            // Console.WriteLine($"p4.ToString()={p4.ToString()}");
            // Console.WriteLine();
            // STPoint<string> p5 = default;
            // Console.WriteLine($"p5.ToString()={p5.ToString()}");

            // Console.ReadLine();
            #endregion

            #region Multi Indexer With Data Table
            // // Make a simple DataTable with 3 columns.
            // DataTable myTable = new DataTable();
            // myTable.Columns.Add(new DataColumn("FirstName"));
            // myTable.Columns.Add(new DataColumn("LastName"));
            // myTable.Columns.Add(new DataColumn("Age"));

            // // Now add a row to the table.
            // myTable.Rows.Add("Mel", "Appleby", 60);

            // // Use multidimension indexer to get details of first row.
            // Console.WriteLine($"First Name: {myTable.Rows[0][0]}");
            // Console.WriteLine($"Last Name: {myTable.Rows[0][1]}");
            // Console.WriteLine($"Age : {myTable.Rows[0][2]}");
            #endregion

            // // var Plus = Calculator.Plus(25, 45);
            // // var Minus = Calculator.Minus(96, 11);
            // // var Multiply = Calculator.Multiply(4);
            // // var Divide = Calculator.Divide(8771, 5);
            // var UseParam = Calculator.UseParam(1, 2, 3);

            // // Console.WriteLine(Plus);
            // // Console.WriteLine(Minus);
            // // Console.WriteLine(Multiply);
            // Console.WriteLine(UseParam);
            // int aa = 73;
            // Increment(out aa);
            // Console.WriteLine(aa);

            // var d = "11";
            // int output;

            // int.TryParse(d, out output);
            // Console.WriteLine(output);

            // Person p = new Person("j", "k", 44);
            // Console.WriteLine(p.ToString());

            // IncreaseInc(ref p);
            // Console.WriteLine(p.ToString());

            // Point pt = new Point(1, 4);


            // IncrementStruct(ref pt);
            // Console.WriteLine(pt.ToString());
            // List<int> list = new List<int>{1,2,3,4,5,6,7,8,9};
            // list.ForEach(f=>{
            //     Console.WriteLine(f);
            // });
            // var tt = YieldTest();
            // foreach (var item in tt.Take(3))
            // {
            //     Console.WriteLine("mainI " + item);
            // }

            // Console.WriteLine();

            // foreach (var item in tt)
            // {
            //     Console.WriteLine("mainII " + item);
            // }

            #region overloaded
            // //Arithmetic Operators
            // FractionArithmeticOperators aI = new FractionArithmeticOperators(5, 4);
            // FractionArithmeticOperators bI = new FractionArithmeticOperators(1, 2);

            // Console.WriteLine(-aI);    // Output: -5 / 4
            // Console.WriteLine(aI + bI); // Output: 14 / 8
            // Console.WriteLine(aI - bI); // Output: 6 / 8
            // Console.WriteLine(aI * bI); // Output: 5 / 8
            // Console.WriteLine(aI / bI); // Output: 10 / 4

            // Console.WriteLine();

            // //Equality Operator
            // var aII = new FractionEqualityOperators(5, 4);
            // var bII = new FractionEqualityOperators(1, 2);

            // Console.WriteLine($"Are fractions a and b equal? {aII == bII}"); // Output: False
            // Console.WriteLine($"Are fractions a and b not equal? {aII != bII}"); // Output: True

            // Console.WriteLine();

            // //Comparison Operators
            // var aIII = new ComplexNumber(3, 5);
            // var bIII = new ComplexNumber(2, 4);

            // Console.WriteLine($"Point 1: {aIII}");
            // Console.WriteLine($"Point 2: {bIII}");

            // // Compare points using the implemented CompareTo method
            // Console.WriteLine($"Is aIII greater than bIII? {aIII > bIII}");
            // Console.WriteLine($"Is aIII less than bIII? {aIII < bIII}");
            // Console.WriteLine($"Is aIII greater than or equal to bIII? {aIII >= bIII}");
            // Console.WriteLine($"Is aIII less than or equal to bIII? {aIII <= bIII}");
            #endregion

            #region Extension Method
            // Console.WriteLine("***** Fun with Extension Methods *****\n");

            // // The int has assumed a new identity!
            // int myInt = 12345678;
            // myInt.DisplayDefiningAssembly();

            // // So has the DataSet!
            // System.Data.DataSet d = new System.Data.DataSet();
            // d.DisplayDefiningAssembly();

            // // Use new integer functionality.
            // Console.WriteLine($"Value of myInt: {myInt}");
            // Console.WriteLine($"Reversed digits of myInt: {myInt.ReverseDigits()}");
            // Console.ReadLine();

            // //----------------------------------------------------------------------------------------------------------------------------------------------

            // //type implement specific interface
            // Console.WriteLine("***** Extending Interface Compatible Types *****\n");
            // // System.Array implements IEnumerable!
            // string[] data = { "Wow", "this", "is", "sort", "of", "annoying", "but", "in", "a", "weird", "way", "fun!" };
            // data.PrintDataAndBeep();
            // Console.WriteLine();

            // // List<T> implements IEnumerable!
            // List<int> myInts = new List<int>() { 10, 15, 20 };
            // myInts.PrintDataAndBeep();
            // Console.ReadLine();
            #endregion

            #region Anonymous Type
            // Console.WriteLine("***** Fun with Anonymous Types *****\n");

            // // Make an anonymous type representing a car.
            // var myCar = new { Color = "Bright Pink", Make = "Saab", CurrentSpeed = 55 };

            // // Now show the color and make.
            // Console.WriteLine($"My car is a {myCar.Color} {myCar.Make}.");

            // // Now call our helper method to build anonymous type via args.
            // BuildAnonymousType("BMW", "Black", 90);
            // Console.WriteLine();

            // EqualityTest();
            #endregion

            #region Swap Testing
            // int n1 = 2;
            // int n2 = 55;
            // SwapFunc(ref n1, ref n2);

            // Console.WriteLine($"this is n1: {n1} and this is n2: {n2}");
            #endregion

            #region Events
            // Console.WriteLine("***** Agh! No Encapsulation! *****\n");
            // // Make a Car.
            // CarDelegate myCar = new CarDelegate();

            // // We have direct access to the delegate!
            // myCar.ListOfHandlers = CallWhenExploded;
            // myCar.Accelerate(10);

            // // We can now assign to a whole new object...
            // // confusing at best.
            // myCar.ListOfHandlers = CallHereToo;
            // myCar.Accelerate(10);

            // // The caller can also directly invoke the delegate!
            // myCar.ListOfHandlers.Invoke("hee, hee, hee...");
            // Console.WriteLine();


            // //using envent
            // var cEvent = new CarEvents();
            // cEvent.AboutToBlow += CallHereToo;
            // cEvent.Exploded += CallWhenExploded;

            // for (int i = 0; i <= 7; i++)
            // {
            //     cEvent.Accelerate(15);
            // }
            #endregion

            // static void PerformTask()
            // {
            //     Console.WriteLine("New thread started to perform the task.");

            //     // Simulating a task that takes some time to complete
            //     for (int i = 0; i < 5; i++)
            //     {
            //         Console.WriteLine("Task is executing...");
            //         Thread.Sleep(2000); // Simulating some work
            //     }

            //     Console.WriteLine("Task completed.");
            // }

            #region Rambda expression
            // Console.WriteLine("******** Discards with Anonymous Methods ********");
            // // Func<int, int, int> constant = delegate (int _, int _) { return 42; };
            // // Console.WriteLine($"constant(3,4) = {constant(3, 4)}");

            // //the AnonymousMethodSyntax() vs LambdaExpressionSyntaxI() vs LambdaExpressionSyntaxII()
            // // AnonymousMethodSyntax();
            // // LambdaExpressionSyntaxI();
            // // LambdaExpressionSyntaxII();

            // // Register with delegate as a lambda expression.
            // SimpleMath m = new SimpleMath();
            // m.SetMathHandler((msg, result) =>
            // {
            //     Console.WriteLine($"Message: {msg}, Result: {result}");
            // });

            // //the syntax above and below are the same
            // m.SetMathHandler((string msg, int result) =>
            // {
            //     Console.WriteLine($"Message: {msg}, Result: {result}");
            // });

            // // This will execute the lambda expression.
            // m.Add(10, 10);
            // Console.WriteLine();
            #endregion

            #region LINQ
            // QueryOverStrings();
            // QueryOverInts();
            // DefaultWhenEmpty();
            // ImmediateExecution();
            // OfTypeAsFilter();

            // Console.WriteLine("***** Fun with Query Expressions *****\n");

            // // This array will be the basis of our testing...
            // ProductInfo[] itemsInStock = new[] {
            //     new ProductInfo{ Name = "Mac's Coffee", Description = "Coffee with TEETH", NumberInStock = 24},
            //     new ProductInfo{ Name = "Milk Maid Milk", Description = "Milk cow's love", NumberInStock = 100},
            //     new ProductInfo{ Name = "Pure Silk Tofu", Description = "Bland as Possible", NumberInStock = 120},
            //     new ProductInfo{ Name = "Crunchy Pops", Description = "Cheezy, peppery goodness", NumberInStock = 2},
            //     new ProductInfo{ Name = "RipOff Water", Description = "From the tap to your wallet", NumberInStock = 100},
            //     new ProductInfo{ Name = "Classic Valpo Pizza", Description = "Everyone loves pizza!", NumberInStock = 73}
            // };

            // // We will call various methods here!
            // Console.ReadLine();


            //LINQ exercise
            // List<int> numbers = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            // int sumOfEvenNumbers = 0;
            // //use foreach
            // foreach (var number in numbers)
            // {
            //     if (number % 2 == 0)
            //     {
            //         sumOfEvenNumbers += number;
            //     }
            // }
            // Console.WriteLine(sumOfEvenNumbers);

            // //use linq
            // var output = numbers.Where(w => w % 2 == 0);
            // int sumOutput = output.Sum();
            // Console.WriteLine(sumOutput);

            // //---------------------------------------------------------------------------------------------------------------------------------------

            // List<string> fruits = new List<string> { "apple", "banana", "cherry", "date", "grape", "apple", "cherry", "banana", "grape" };

            // //use foreach
            // Dictionary<string, int> fruitCount = new Dictionary<string, int>();

            // foreach (var fruit in fruits)
            // {
            //     if (fruitCount.ContainsKey(fruit))
            //     {
            //         fruitCount[fruit]++;
            //     }
            //     else
            //     {
            //         fruitCount.Add(fruit, 1);
            //     }
            // }

            // foreach (var item in fruitCount)
            // {
            //     Console.WriteLine($"{item.Key}: {item.Value}");
            // }

            // //use linq
            // var CountApple = fruits.Where(s => s == "apple").Count();
            // var CountBanana = fruits.Where(s => s == "banana").Count();
            // var CountCherry = fruits.Where(s => s == "cherry").Count();
            // var CountDate = fruits.Where(s => s == "date").Count();
            // var CountGrape = fruits.Where(s => s == "grape").Count();


            // string Apple = fruits.First(f => f == "apple");
            // string Banana = fruits.First(f => f == "banana");
            // string Cherry = fruits.First(f => f == "cherry");
            // string Date = fruits.First(f => f == "date");
            // string Grape = fruits.First(f => f == "grape");

            // Console.WriteLine($"\n{Apple}: {CountApple} \n{Banana}: {CountBanana} \n{Cherry}: {CountCherry} \n{Date}: {CountDate} \n{Grape}: {CountGrape}");
            // //---------------------------------------------------------------------------------------------------------------------------------------

            // List<string> names = new List<string> { "John", "Jane", "Billy", "Doe", "Jane", "John" };

            // //use foreach
            // List<string> uniqueSortedNames = new List<string>();

            // foreach (var name in names)
            // {
            //     if (!uniqueSortedNames.Contains(name))
            //     {
            //         uniqueSortedNames.Add(name);
            //     }
            // }
            // uniqueSortedNames.Sort();

            // foreach (var name in uniqueSortedNames)
            // {
            //     Console.WriteLine(name);
            // }

            // //use linq
            // var ResultName = names.OrderBy(o => o).GroupBy(g => g).ToList();

            // Console.WriteLine();
            // foreach (var i in ResultName)
            // {
            //     Console.WriteLine($"{i.Key}");
            // }
            // //---------------------------------------------------------------------------------------------------------------------------------------

            // List<List<int>> matrix = new List<List<int>>
            // {
            //     new List<int> { 1, 2, 3 },
            //     new List<int> { 4, 5, 6 },
            //     new List<int> { 7, 8, 9, 10 }
            // };
            // List<int> evenNumbers = new List<int>();

            // //use foreach
            // foreach (var row in matrix)
            // {
            //     foreach (var item in row)
            //     {
            //         if (item % 2 == 0)
            //         {
            //             evenNumbers.Add(item);
            //         }
            //     }
            // }

            // foreach (var num in evenNumbers)
            // {
            //     Console.WriteLine(num);
            // }

            // //use linq
            // var a = matrix.Select(row =>
            // {
            //     return row.Where(item => item % 2 == 0);
            // });

            // var Result = matrix.SelectMany(s => s).Where(w => w % 2 == 0).ToList();

            // Console.WriteLine();
            // foreach (var f in a)
            // {
            //     Console.WriteLine(f);
            // }

            // //---------------------------------------------------------------------------------------------------------------------------------------

            // List<Employee> employees = new List<Employee>
            // {
            //     new Employee { Name = "John Doe", Department = "IT", Salary = 60000 },
            //     new Employee { Name = "Jane Doe", Department = "HR", Salary = 65000 },
            //     new Employee { Name = "Jim Beam", Department = "IT", Salary = 55000 },
            //     new Employee { Name = "Zara Larsson", Department = "Marketing", Salary = 75000 }
            // };

            // //use foreach
            // List<string> itDepartmentEmployeeNames = new List<string>();
            // foreach (var employee in employees)
            // {
            //     if (employee.Department == "IT" && employee.Salary > 50000)
            //     {
            //         itDepartmentEmployeeNames.Add(employee.Name.ToUpper());
            //     }
            // }

            // foreach (var name in itDepartmentEmployeeNames)
            // {
            //     Console.WriteLine(name);
            // }

            // //line break
            // Console.WriteLine();

            // //use linq
            // var Result = employees.Where(w => w.Department.ToLower() == "it" && w.Salary > 50000).Select(s => s.Name.ToUpper()).ToList();

            // foreach (var item in Result)
            // {
            //     Console.WriteLine(item);
            // }
            #endregion

            // var text1 = "abc";
            // var text2 = "abc";

            // Console.WriteLine(ReferenceEquals(text1, text2));


            // char[] arr = new char[26];

            // string str = string.Empty;
            // var result = new StringBuilder();

            // var stopwatch = new Stopwatch();
            // stopwatch.Start();
            // for (var i = 'A'; i <= 'Z'; i++)
            // {
            // str += i;
            // }
            // for (var i = 'A'; i <= 'Z'; i++)
            // {
            // result.Append(i);
            // }
            // stopwatch.Stop();
            // Console.WriteLine(result);
            // Console.WriteLine(stopwatch.ElapsedTicks.ToString());


            // stopwatch.Restart();
            // for (var i = 0; i < 26; i++)
            // {
            // arr[i] = (char)('A' + i);
            // }
            // stopwatch.Stop();
            // Console.WriteLine(arr);
            // Console.WriteLine(stopwatch.ElapsedTicks.ToString());

            // string a = "qwer";
            // string aa = a + "ee";
            // Console.WriteLine(a.GetHashCode());
            // Console.WriteLine(aa.GetHashCode());
            // // Console.WriteLine(aa.ToUpper().GetHashCode());


            #region Process Thread
            // ListAllRunningProcesses();

            // // Prompt user for a PID and print out the set of active threads.
            // Console.WriteLine("***** Enter PID of process to investigate *****");
            // Console.Write("PID: ");
            // string pID = Console.ReadLine();
            // int theProcID = int.Parse(pID);
            // EnumThreadsForPid(theProcID);
            // Console.ReadLine();

            // Console.WriteLine("***** Enter PID of process to investigate *****");
            // Console.Write("PID: ");
            // string pID = Console.ReadLine();
            // int theProcID = int.Parse(pID);
            // EnumModsForPid(theProcID);
            // Console.ReadLine();

            // StartAndKillProcess();
            // UseApplicationVerbs();

            // Console.WriteLine("***** Fun with the default AppDomain *****\n");
            // DisplayDADStats();
            // Console.ReadLine();

            // ListAllAssembliesInAppDomain();

            // //  Thread currThread = Thread.CurrentThread;
            // AppDomain ad = Thread.GetDomain();
            // ExecutionContext ctx = Thread.CurrentThread.ExecutionContext;

            // //  Console.WriteLine($"currThread: {currThread}");
            // Console.WriteLine($"{ad}");
            // Console.WriteLine($"{ctx}");
            #endregion

            #region silence error
            // //silence error
            // int a = 2_000_000_000; //context out of range, use long or something bigger than int
            // int b = 2_000_000_000;

            // int c = checked(a + b);
            // Console.WriteLine(a + b);

            // bool ss = (0.2d + 0.1d) == 0.3d; //result = false cuz float and double, use decimal instead of those variable

            // Console.WriteLine(ss);
            #endregion

            #region AddParam
            // Console.WriteLine("***** Adding with Thread objects *****");
            // Console.WriteLine($"ID of thread in Main(): {Environment.CurrentManagedThreadId}");

            // // Make an AddParams object to pass to the secondary thread.
            // AddParams ap = new AddParams(10_000, 10_000);
            // Thread t = new Thread(new ParameterizedThreadStart(Add));
            // t.Start(ap);

            // // Force a wait to let other thread finish.
            // Thread.Sleep(5);
            // Console.WriteLine();
            #endregion

            #region lock keyword
            // Console.WriteLine("***** Fun with the .NET Core Runtime Thread Pool *****\n");
            // Console.WriteLine($"Main thread started. ThreadID = {Environment.CurrentManagedThreadId}");
            // Printer p = new Printer();
            // WaitCallback workItem = new WaitCallback(PrintTheNumbers);

            // // Queue the method ten times.
            // for (int i = 0; i < 10; i++)
            // {
            //     ThreadPool.QueueUserWorkItem(workItem, p);
            // }
            // Console.WriteLine("All tasks queued");
            // Console.ReadLine();
            #endregion

            #region Structs
            // EmpStructs emp = new EmpStructs();
            // emp.Id = 1;
            // emp.Name = "Mathew Smith";
            // emp.Address = "123, abc rd. aaa, bbb, thailand";

            // emp.GetEmp();
            #endregion

            #region interface polymorphism
            // ICalculateSomething meal = new Meal { Pasta = 200 };
            // ICalculateSomething drink = new Beverages { Coke = 40, Milk = 60 };

            // Console.WriteLine($"Pasta for two people: {meal.Calculator()}");
            // Console.WriteLine($"Beverages: {drink.Calculator()}");
            #endregion

            #region call by value vs call by reference
            // int num = 10;
            // Console.WriteLine($"The original value: {num}");

            // //call by value
            // DoubleValue(num);
            // Console.WriteLine($"Outside method: num = {num}");

            // //call by reference
            // DoubleRef(ref num);
            // Console.WriteLine($"Outside method: num = {num}");
            #endregion

            #region ref keyword
            // Complex C = new Complex(2, 4);

            // Console.WriteLine($"Complex number C = {C.Real}");
            // Console.WriteLine($"Complex number i = {C.Imaginary}");
            // Console.WriteLine($"Complex number C + i = {C.Real} + {C.Imaginary}");

            // Update(ref C);

            // Console.WriteLine("\nAfter updating C\n");

            // Console.WriteLine($"Complex number C = {C.Real}");
            // Console.WriteLine($"Complex number i = {C.Imaginary}");
            // Console.WriteLine($"Complex number C + i = {C.Real} + {C.Imaginary}");
            #endregion

            #region out keyword
            // double radiusValue = 3.92781;

            // CalculateCircumferenceAndArea(radiusValue, out double circumferenceResult, out double areaResult);

            // Console.WriteLine($"Circumference of a circle with a radius of '{radiusValue}' is {circumferenceResult}.");
            // Console.WriteLine($"Area of a circle with a radius of '{radiusValue}' is {areaResult}.");
            #endregion

            //alphabet war
            AlphabetWar("tjmjttwwme");
        }

        #region alphabet war
        public static void AlphabetWar(string fight)
        {
            var LeftSidePower = new Dictionary<char, int> {
                {'w', 4},
                {'p', 3},
                {'b', 2},
                {'s', 1},
                {'t', 0} // Priest with Wo lo loooooooo power
            };

            var RightSidePower = new Dictionary<char, int> {
                {'m', 4},
                {'q', 3},
                {'d', 2},
                {'z', 1},
                {'j', 0} // Priest with Wo lo loooooooo power
            };

            //total power for both side
            int LeftTotalPower = 0;
            int RightTotalPower = 0;
            bool ChangedNext = false;
            bool FoundNextJ = false;
            bool FoundNextT = false;
            bool FoundPreviousJ = false;
            bool FoundPreviousT = false;

            for (int i = 0; i < fight.Length; i++)
            {
                char CurrentLetter = fight[i];

                if (LeftSidePower.ContainsKey(CurrentLetter))
                {
                    if (i < fight.Length - 2)
                    {
                        if (fight[i + 2] == 'j' || fight[i + 1] == 'j')
                        {
                            FoundNextJ = true;
                        }
                    }

                    if (i > 1)
                    {
                        if (fight[i - 2] == 'j')
                        {
                            FoundPreviousJ = true;
                        }
                    }

                    if (CurrentLetter == 't')
                    {
                        ChangedNext = false;
                        if ((i == 0 && !LeftSidePower.ContainsKey(CurrentLetter)) || (i < fight.Length - 1 && RightSidePower.ContainsKey(fight[i + 1]) && !FoundNextJ))
                        {
                            LeftTotalPower += RightSidePower[fight[i + 1]];
                            ChangedNext = true;
                        }

                        if (i > 0 && RightSidePower.ContainsKey(fight[i - 1]) && !FoundPreviousJ)
                        {
                            LeftTotalPower += RightSidePower[fight[i - 1]];
                        }

                        if (ChangedNext)
                            i++;
                    }
                    else if (i == fight.Length - 1 || (i < fight.Length - 1 && fight[i + 1] != 'j') || (fight[i + 1] == 'j' && (i > 0 && fight[i - 1] == 't')))
                    {
                        LeftTotalPower += LeftSidePower[CurrentLetter];
                    }
                }

                if (RightSidePower.ContainsKey(CurrentLetter))
                {
                    if (i < fight.Length - 2)
                    {
                        if (fight[i + 2] == 't' || fight[i + 1] == 't')
                        {
                            FoundNextT = true;
                        }
                    }

                    if (i > 1)
                    {
                        if (fight[i - 2] == 't')
                        {
                            FoundPreviousT = true;
                        }
                    }

                    if (CurrentLetter == 'j')
                    {
                        ChangedNext = false;
                        if ((i == 0 && !RightSidePower.ContainsKey(CurrentLetter)) || (i < fight.Length - 1 && LeftSidePower.ContainsKey(fight[i + 1]) && !FoundNextT))
                        {
                            RightTotalPower += LeftSidePower[fight[i + 1]];
                            ChangedNext = true;
                        }

                        if (i > 0 && LeftSidePower.ContainsKey(fight[i - 1]) && !FoundPreviousT)
                        {
                            RightTotalPower += LeftSidePower[fight[i - 1]];
                        }

                        if (ChangedNext)
                            i++;
                    }
                    else if (i == fight.Length - 1 || (i < fight.Length - 1 && fight[i + 1] != 't') || (fight[i + 1] == 't' && (i > 0 && fight[i - 1] == 'j')))
                    {
                        RightTotalPower += RightSidePower[CurrentLetter];
                    }
                }
            }

            // Determine the winner
            if (LeftTotalPower > RightTotalPower)
            {
                Console.WriteLine("Left side wins!");
                // return "Left side wins!";
            }
            else if (RightTotalPower > LeftTotalPower)
            {
                Console.WriteLine("Right side wins!");
                // return "Right side wins!";
            }
            else
            {
                Console.WriteLine("Let's fight again!");
                // return "Let's fight again!";
            }
        }
        #endregion

        static void CalculateCircumferenceAndArea(double radius, out double circumference, out double area)
        {
            circumference = 2 * Math.PI * radius;
            area = Math.PI * (radius * radius);
        }

        static void Update(ref Complex obj)
        {
            obj.Real += 5;
            obj.Imaginary += 5;
        }

        static void DoubleRef(ref int x)
        {
            x *= 2;
            Console.WriteLine($"Inside method: x = {x}");
        }

        static void DoubleValue(int x)
        {
            x *= 2;
            Console.WriteLine($"Inside method: x = {x}");
        }

        static void PrintTheNumbers(object? state)
        {
            Printer task = (Printer)state;
            task?.PrintNumbers();
        }

        #region AddParam
        static void Add(object? data)
        {
            if (data is AddParams ap)
            {
                Console.WriteLine($"ID of thread in Add(): {Environment.CurrentManagedThreadId}");
                Console.WriteLine($"{ap.a} + {ap.b} is {ap.a + ap.b}");
            }
        }
        #endregion

        #region Process Thread
        static void ListAllRunningProcesses()
        {
            // Get all the processes on the local machine, ordered by
            // PID. 
            var runningProcs = from proc in Process.GetProcesses(".")
                               orderby proc.Id
                               select proc;
            // Print out PID and name of each process.
            foreach (var p in runningProcs)
            {
                string info = $"-> PID: {p.Id}\tName: {p.ProcessName}";
                Console.WriteLine(info);
            }
            Console.WriteLine("************************************\n");
        }

        // If there is no process with the PID of 30592, a runtime exception will be thrown.
        static void GetSpecificProcess()
        {
            Process theProc = null;
            try
            {
                theProc = Process.GetProcessById(30592);
                Console.WriteLine(theProc?.ProcessName);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        static void EnumThreadsForPid(int pID)
        {
            Process theProc = null;
            try
            {
                theProc = Process.GetProcessById(pID);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }

            // List out stats for each thread in the specified process.
            Console.WriteLine(
            "Here are the threads used by: {0}", theProc.ProcessName);
            ProcessThreadCollection theThreads = theProc.Threads;
            foreach (ProcessThread pt in theThreads)
            {
                string info = $"-> Thread ID: {pt.Id}\tStart Time: {pt.StartTime.ToShortTimeString()}\tPriority: {pt.PriorityLevel}";
                Console.WriteLine(info);
            }
            Console.WriteLine("************************************\n");
        }

        static void EnumModsForPid(int pID)
        {
            Process theProc = null;
            try
            {
                theProc = Process.GetProcessById(pID);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }
            Console.WriteLine("Here are the loaded modules for: {0}",
            theProc.ProcessName);
            ProcessModuleCollection theMods = theProc.Modules;
            foreach (ProcessModule pm in theMods)
            {
                string info = $"-> Mod Name: {pm.ModuleName}";
                Console.WriteLine(info);
            }
            Console.WriteLine("************************************\n");
        }

        static void StartAndKillProcess()
        {
            Process proc = null;
            // Launch Edge, and go to Facebook!
            try
            {
                proc = Process.Start(@"C:\Program Files (x86)\Microsoft\Edge\Application\msedge.exe", "www.facebook.com");
            }
            catch (InvalidOperationException ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.Write($"--> Hit enter to kill {proc.ProcessName}...");
            Console.ReadLine();

            // Kill all of the msedge.exe processes.
            try
            {
                foreach (var p in Process.GetProcessesByName("MsEdge"))
                {
                    p.Kill(true);
                }
            }
            catch (InvalidOperationException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        static void UseApplicationVerbs()
        {
            int i = 0;
            //adjust this path and name to a document on your machine
            ProcessStartInfo si = new ProcessStartInfo(@"..\TestPage.docx");

            foreach (var verb in si.Verbs)
            {
                Console.WriteLine($" {i++}. {verb}");
            }

            si.WindowStyle = ProcessWindowStyle.Maximized;
            si.Verb = "Edit";
            si.UseShellExecute = true;
            Process.Start(si);
        }

        static void DisplayDADStats()
        {
            // Get access to the AppDomain for the current thread.
            AppDomain defaultAD = AppDomain.CurrentDomain;

            // Print out various stats about this domain.
            Console.WriteLine($"Name of this domain: {defaultAD.FriendlyName}");
            Console.WriteLine($"ID of domain in this process: {defaultAD.Id}");
            Console.WriteLine($"Is this the default domain?: {defaultAD.IsDefaultAppDomain()}");
            Console.WriteLine($"Base directory of this domain: {defaultAD.BaseDirectory}");
            Console.WriteLine($"Setup Information for this domain:");
            Console.WriteLine($"\t Application Base: {defaultAD.SetupInformation.ApplicationBase}");
            Console.WriteLine($"\t Target Framework: {defaultAD.SetupInformation.TargetFrameworkName}");
        }

        static void ListAllAssembliesInAppDomain()
        {
            // Get access to the AppDomain for the current thread.
            AppDomain defaultAD = AppDomain.CurrentDomain;
            // Now get all loaded assemblies in the default AppDomain.
            Assembly[] loadedAssemblies = defaultAD.GetAssemblies();
            Console.WriteLine("***** Here are the assemblies loaded in {0} *****\n",
            defaultAD.FriendlyName);
            foreach (Assembly a in loadedAssemblies)
            {
                Console.WriteLine($"-> Name, Version: {a.GetName().Name}:{a.GetName().Version}");
            }
        }
        #endregion

        #region LINQ
        class Employee
        {
            public string Name { get; set; }
            public string Department { get; set; }
            public int Salary { get; set; }
        }

        static void PagingWithLINQ(ProductInfo[] products)
        {
            Console.WriteLine("Paging Operations");
            IEnumerable<ProductInfo> list = (from p in products select p).Take(3);
            OutputResults("The first 3", list);
            static void OutputResults(string message, IEnumerable<ProductInfo> products)
            {
                Console.WriteLine(message);
                foreach (ProductInfo c in products)
                {
                    Console.WriteLine(c.ToString());
                }
            }
        }

        //Get only item that more than 25
        static void GetOverstock(ProductInfo[] products)
        {
            Console.WriteLine("The overstock items!");
            // Get only the items where we have more than
            // 25 in stock.
            var overstock = from p in products
                            where p.NumberInStock > 25
                            select p;

            foreach (ProductInfo c in overstock)
            {
                Console.WriteLine(c.ToString());
            }
        }

        //Get only name of the products
        static void ListProductNames(ProductInfo[] products)
        {
            // Now get only the names of the products.
            Console.WriteLine("Only product names:");
            var names = from p in products
                        select p.Name;

            foreach (var n in names)
            {
                Console.WriteLine("Name: {0}", n);
            }
        }

        //Get everything in products
        static void SelectEverything(ProductInfo[] products)
        {
            // Get everything!
            Console.WriteLine("All product details:");
            var allProducts = from p in products
                              select p;

            foreach (var prod in allProducts)
            {
                Console.WriteLine(prod.ToString());
            }
        }

        static void OfTypeAsFilter()
        {
            // Extract the ints from the ArrayList.
            ArrayList myStuff = new ArrayList();
            myStuff.AddRange(new object[] { 10, 400, 8, false, new Car(), "string data" });
            var myInts = myStuff.OfType<int>(); //filter only int

            // Prints out 10, 400, and 8.
            foreach (int i in myInts)
            {
                Console.WriteLine($"Int value: {i}");
            }
        }

        static void GetFastCars(List<Car> myCars)
        {
            // Find all Car objects in the List<>, where the Speed is
            // greater than 55.
            var fastCars = from c in myCars where c.CurrentSpeed > 55 select c;
            foreach (var car in fastCars)
            {
                Console.WriteLine("{0} is going too fast!", car.PetName);
            }
        }

        static void ImmediateExecution()
        {
            Console.WriteLine();
            Console.WriteLine("Immediate Execution");
            int[] numbers = { 10, 20, 30, 40, 1, 2, 3, 8 };

            //get the first element in sequence order
            int number = (from i in numbers select i).First();
            Console.WriteLine($"First is {number}");

            //get the first in query order
            number = (from i in numbers orderby i select i).First();
            Console.WriteLine($"First is {number}");

            //get the one element that matches the query
            number = (from i in numbers where i > 30 select i).Single();
            Console.WriteLine($"Single is {number}");

            //Return null if nothing is returned
            number = (from i in numbers where i > 99 select i).FirstOrDefault();
            number = (from i in numbers where i > 99 select i).SingleOrDefault();

            Console.WriteLine();
            try
            {
                //Throw an exception if no records returned
                number = (from i in numbers where i > 99 select i).First();
            }
            catch (Exception ex)
            {
                //  Exception: Sequence contains no elements
                Console.WriteLine($"An exception occurred: {ex.Message}");
            }

            try
            {
                //Throw an exception if no records returned
                number = (from i in numbers where i > 99 select i).Single();
            }
            catch (Exception ex)
            {
                //Exception: Sequence contains no elements
                Console.WriteLine($"An exception occurred: {ex.Message}");
            }

            try
            {
                //Throw an exception if more than one element passes the query
                number = (from i in numbers where i > 10 select i).Single();
            }
            catch (Exception ex)
            {
                //Exception: Sequence contains more than one element
                Console.WriteLine($"An exception occurred: {ex.Message}");
            }

            // Get data RIGHT NOW as int[].
            int[] subsetAsIntArrayI = (from i in numbers where i < 10 select i).ToArray<int>();

            //Can use .ToArray();
            int[] subsetAsIntArrayII = (from i in numbers where i < 10 select i).ToArray();

            // Get data RIGHT NOW as List<int>.
            List<int> subsetAsListOfInts = (from i in numbers where i < 10 select i).ToList<int>();
        }

        static void DefaultWhenEmpty()
        {
            Console.WriteLine("Default When Empty");
            int[] numbers = { 10, 20, 30, 40, 1, 2, 3, 8 };

            //Returns all of the numbers
            foreach (var i in numbers.DefaultIfEmpty(-1))
            {
                Console.Write($"{i},");
            }
            Console.WriteLine();

            //Returns -1 since the sequence is empty
            foreach (var i in (from i in numbers where i > 99 select i).DefaultIfEmpty(-1))
            {
                Console.Write($"{i},");
            }
            Console.WriteLine();
        }

        static void QueryOverInts()
        {
            int[] numbers = { 10, 20, 30, 40, 1, 2, 3, 8 };

            // Get numbers less than ten.
            var subset = from i in numbers where i < 10 select i;

            // LINQ statement evaluated here!
            foreach (var i in subset)
            {
                Console.WriteLine($"{i} < 10");
            }
            Console.WriteLine();

            // Change some data in the array.
            numbers[0] = 4;

            // Evaluated again!
            foreach (var j in subset)
            {
                Console.WriteLine($"{j} < 10");
            }
            Console.WriteLine();
            ReflectOverQueryResults(subset);
        }

        static void ReflectOverQueryResults(object resultSet, string queryType = "Query Expressions")
        {
            Console.WriteLine($"***** Info about your query using {queryType} *****");
            Console.WriteLine($"resultSet is of type: {resultSet.GetType().Name}");
            Console.WriteLine($"resultSet location: {resultSet.GetType().Assembly.GetName().Name}");
        }

        static void QueryOverStrings()
        {
            // Assume we have an array of strings.
            string[] currentVideoGames = { "Morrowind", "Uncharted 2", "Fallout 3", "Daxter", "System Shock 2" };

            // Build a query expression to find the items in the array
            // that have an embedded space.
            IEnumerable<string> subsetI = from g in currentVideoGames
                                          where g.Contains(" ")
                                          orderby g
                                          select g;

            ReflectOverQueryResults(subsetI);
            Console.WriteLine();

            ReflectOverQueryResults(subsetI, "Extension Methods");
            Console.WriteLine();

            // Using Extension Methods
            // Build a query expression to find the items in the array
            // that have an embedded space.
            IEnumerable<string> subsetII = currentVideoGames.Where(g => g.Contains(" ")).OrderBy(g => g).Select(g => g);

            // Print out the results.
            foreach (string s in subsetI)
            {
                Console.WriteLine($"Item: {s}");
            }
            Console.WriteLine();
        }
        #endregion

        #region Lambda Expression
        static void AnonymousMethodSyntax()
        {
            // Make a list of integers.
            List<int> list = new List<int>();
            list.AddRange(new int[] { 20, 1, 4, 8, 9, 44 });

            // Now, use an anonymous method.
            List<int> evenNumbers =
            list.FindAll(delegate (int i) { return (i % 2) == 0; }); //inshort use lambda expression -> (i => (i % 2) == 0);

            Console.WriteLine("Here are your even numbers:");
            foreach (int evenNumber in evenNumbers)
            {
                Console.WriteLine($"{evenNumber}\t");
            }
            Console.WriteLine();
        }

        static void LambdaExpressionSyntaxI()
        {
            // Make a list of integers.
            List<int> list = new List<int>();
            list.AddRange(new int[] { 20, 1, 4, 8, 9, 44 });

            // Now, use a C# lambda expression.
            // "i" is our parameter list.
            // "(i % 2) == 0" is our statement set to process "i
            List<int> evenNumbers = list.FindAll(i => (i % 2) == 0); //from (delegate (int i) {return (i % 2) == 0; });

            Console.WriteLine("Here are your even numbers:");
            foreach (int evenNumber in evenNumbers)
            {
                Console.WriteLine($"{evenNumber}\t");
            }
            Console.WriteLine();
        }

        static void LambdaExpressionSyntaxII()
        {
            // Make a list of integers.
            List<int> list = new List<int>();
            list.AddRange(new int[] { 20, 1, 4, 8, 9, 44 });

            // Now process each argument within a group of
            // code statements.
            List<int> evenNumbers = list.FindAll((i) =>
            {
                Console.WriteLine("value of i is currently: {0}", i);
                bool isEven = ((i % 2) == 0);
                return isEven;
            });

            Console.WriteLine("Here are your even numbers:");
            foreach (int evenNumber in evenNumbers)
            {
                Console.WriteLine($"{evenNumber}\t");
            }
            Console.WriteLine();
        }
        #endregion

        #region car delegate
        static void CallWhenExploded(string msg)
        {
            Console.WriteLine(msg);
        }

        static void CallHereToo(string msg)
        {
            Console.WriteLine(msg);
        }
        #endregion

        static void SwapFunc<T>(ref T a, ref T b)
        {
            T newA = b;
            b = a;
            a = newA;
        }

        static void EqualityTest()
        {
            // Make an anonymous type that is composed of another.
            var purchaseItem = new
            {
                TimeBought = DateTime.Now,
                ItemBought = new
                {
                    Color = "Red",
                    Make = "Saab",
                    CurrentSpeed = 55
                },
                Price = 34.000
            };
            ReflectOverAnonymousType(purchaseItem);

            // Make 2 anonymous classes with identical name/value pairs.
            var firstCar = new { Color = "Bright Pink", Make = "Saab", CurrentSpeed = 55 };
            var secondCar = new { Color = "Bright Pink", Make = "Saab", CurrentSpeed = 55 };

            // Are they considered equal when using Equals()?
            if (firstCar.Equals(secondCar))
            {
                Console.WriteLine("Same anonymous object!");
            }
            else
            {
                Console.WriteLine("Not the same anonymous object!");
            }

            // Are they considered equal when using ==?
            if (firstCar == secondCar)
            {
                Console.WriteLine("Same anonymous object!");
            }
            else
            {
                Console.WriteLine("Not the same anonymous object!");
            }

            // Are these objects the same underlying type?
            if (firstCar.GetType().Name == secondCar.GetType().Name)
            {
                Console.WriteLine("We are both the same type!");
            }
            else
            {
                Console.WriteLine("We are different types!");
            }

            // Show all the details.
            Console.WriteLine();
            ReflectOverAnonymousType(firstCar);
            ReflectOverAnonymousType(secondCar);
        }

        static void ReflectOverAnonymousType(object obj)
        {
            Console.WriteLine($"obj is an instance of: {obj.GetType().Name}");
            Console.WriteLine($"Base class of {obj.GetType().Name} is {obj.GetType().BaseType}");
            Console.WriteLine($"obj.ToString() == {obj.ToString()}");
            Console.WriteLine($"obj.GetHashCode() == {obj.GetHashCode()}");
            Console.WriteLine();
        }

        static void BuildAnonymousType(string make, string color, int currSp)
        {
            // Build anonymous type using incoming args.
            var car = new { Make = make, Color = color, Speed = currSp };
            // Note you can now use this type to get the property data!
            Console.WriteLine($"You have a {car.Color} {car.Make} going {car.Speed} MPH");
            // Anonymous types have custom implementations of each virtual
            // method of System.Object. For example:
            Console.WriteLine($"ToString() == {car.ToString()}");
        }

        static IEnumerable<int> YieldTest()
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("yt " + i);
                yield return i;
            }
        }

        static void Increment(out int a)
        {
            a = 10;
        }

        static void IncreaseInc(ref Person person)
        {
            person.Age += 1;
        }

        static void IncrementStruct(ref Point point)
        {
            point.X += 1;
            point.Y -= 1;
        }

        #region Stack<T>: last-in, first-out
        static void UseGenericStack()
        {
            Stack<Person> stackOfPeople = new();
            stackOfPeople.Push(new Person { FirstName = "Homer", LastName = "Simpson", Age = 47 });
            stackOfPeople.Push(new Person { FirstName = "Marge", LastName = "Simpson", Age = 45 });
            stackOfPeople.Push(new Person { FirstName = "Lisa", LastName = "Simpson", Age = 9 });

            // Now look at the top item, pop it, and look again.
            Console.WriteLine($"First person is: {stackOfPeople.Peek()}");
            Console.WriteLine($"Popped off {stackOfPeople.Pop()}");
            Console.WriteLine($"\nFirst person is: {stackOfPeople.Peek()}");
            Console.WriteLine($"Popped off {stackOfPeople.Pop()}");
            Console.WriteLine($"\nFirst person item is: {stackOfPeople.Peek()}");
            Console.WriteLine($"Popped off {stackOfPeople.Pop()}");
            try
            {
                Console.WriteLine($"\nnFirst person is: {stackOfPeople.Peek()}");
                Console.WriteLine($"Popped off {stackOfPeople.Pop()}"); //remove obj from stack from latest of the list
            }
            catch (InvalidOperationException ex)
            {
                Console.WriteLine($"\nError! {ex.Message}");
            }
        }
        #endregion

        #region Queue<T>
        static void UseGenericQueue()
        {
            // Make a Q with three people.
            Queue<Person> peopleQ = new();
            peopleQ.Enqueue(new Person { FirstName = "Homer", LastName = "Simpson", Age = 47 });
            peopleQ.Enqueue(new Person { FirstName = "Marge", LastName = "Simpson", Age = 45 });
            peopleQ.Enqueue(new Person { FirstName = "Lisa", LastName = "Simpson", Age = 9 });

            // Peek at first person in Q.
            Console.WriteLine($"{peopleQ.Peek().FirstName} is first in line!");

            // Remove each person from Q.
            GetCoffee(peopleQ.Dequeue());
            GetCoffee(peopleQ.Dequeue());
            GetCoffee(peopleQ.Dequeue());

            // Try to de-Q again?
            try
            {
                GetCoffee(peopleQ.Dequeue()); //remove obj from beginning of the queue
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine($"Error! {e.Message}");
            }

            //Local helper function
            static void GetCoffee(Person p)
            {
                Console.WriteLine($"{p.FirstName} got coffee!");
            }
        }
        #endregion

        #region PriorityQueue<T>
        static void UsePriorityQueue()
        {
            Console.WriteLine("* Fun with Generic Priority Queues *\n");
            PriorityQueue<Person, int> peopleQ = new(); //Enqueue obj Person and follow with queue number
            peopleQ.Enqueue(new Person { FirstName = "Lisa", LastName = "Simpson", Age = 9 }, 1);
            peopleQ.Enqueue(new Person { FirstName = "Homer", LastName = "Simpson", Age = 47 }, 3);
            peopleQ.Enqueue(new Person { FirstName = "Marge", LastName = "Simpson", Age = 45 }, 3);
            peopleQ.Enqueue(new Person { FirstName = "Bart", LastName = "Simpson", Age = 12 }, 2);
            while (peopleQ.Count > 0)
            {
                Console.WriteLine(peopleQ.Dequeue().FirstName); //Displays Lisa
                Console.WriteLine(peopleQ.Dequeue().FirstName); //Displays Bart
                Console.WriteLine(peopleQ.Dequeue().FirstName); //Displays either Marge or Homer
                Console.WriteLine(peopleQ.Dequeue().FirstName); //Displays the other priority 3 item
            }
        }
        #endregion

        #region SortedSet<T>
        static void UseSortedSet()
        {
            // Make some people with different ages.
            SortedSet<Person> setOfPeople = new SortedSet<Person>(new SortPeopleByAge())
            {
                new Person {FirstName = "Homer", LastName = "Simpson", Age = 47},
                new Person {FirstName = "Marge", LastName = "Simpson", Age = 45},
                new Person {FirstName = "Lisa", LastName = "Simpson", Age = 9},
                new Person {FirstName = "Bart", LastName = "Simpson", Age = 8}
            };

            // Note the items are sorted by age!
            foreach (Person p in setOfPeople)
            {
                Console.WriteLine(p);
            }
            Console.WriteLine();

            // Add a few new people, with various ages.
            setOfPeople.Add(new Person { FirstName = "Saku", LastName = "Jones", Age = 1 });
            setOfPeople.Add(new Person { FirstName = "Mikko", LastName = "Jones", Age = 32 });

            // Still sorted by age!
            foreach (Person p in setOfPeople)
            {
                Console.WriteLine(p);
            }
        }
        #endregion

        #region Dictionary<TKey, TValue>
        private static void UseDictionary()
        {
            // Populate using Add() method
            Dictionary<string, Person> peopleA = new Dictionary<string, Person>();
            peopleA.Add("Homer", new Person { FirstName = "Homer", LastName = "Simpson", Age = 47 });
            peopleA.Add("Marge", new Person { FirstName = "Marge", LastName = "Simpson", Age = 45 });
            peopleA.Add("Lisa", new Person { FirstName = "Lisa", LastName = "Simpson", Age = 9 });

            // Get Homer.
            Person homer = peopleA["Homer"]; //using key to specific object
            Console.WriteLine(homer);

            // Populate with initialization syntax.
            Dictionary<string, Person> peopleB = new Dictionary<string, Person>()
            {
                { "Homer", new Person { FirstName = "Homer", LastName = "Simpson", Age = 47 } },
                { "Marge", new Person { FirstName = "Marge", LastName = "Simpson", Age = 45 } },
                { "Lisa", new Person { FirstName = "Lisa", LastName = "Simpson", Age = 9 } }
            };

            // Get Lisa.
            Person lisa = peopleB["Lisa"];
            Console.WriteLine(lisa);

            // Populate with dictionary initialization syntax.
            Dictionary<string, Person> peopleC = new Dictionary<string, Person>()
            {
                ["Homer"] = new Person { FirstName = "Homer", LastName = "Simpson", Age = 47 },
                ["Marge"] = new Person { FirstName = "Marge", LastName = "Simpson", Age = 45 },
                ["Lisa"] = new Person { FirstName = "Lisa", LastName = "Simpson", Age = 9 }
            };

            // Get Marge.
            Person Marge = peopleB["Marge"];
            Console.WriteLine(Marge);
        }
        #endregion

        #region ObservableCollection<T>
        static void people_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            // What was the action that caused the event?
            Console.WriteLine($"Action for this event: {e.Action}");
            // They removed something.
            if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                Console.WriteLine("Here are the OLD items:");
                foreach (Person p in e.OldItems)
                {
                    Console.WriteLine(p.ToString());
                }
                Console.WriteLine();
            }
            // They added something.
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                // Now show the NEW items that were inserted.
                Console.WriteLine("Here are the NEW items:");
                foreach (Person p in e.NewItems)
                {
                    Console.WriteLine(p.ToString());
                }
            }
        }
        #endregion

        static void PrintMemoryUsage()
        {
            long memoryUsed = GC.GetTotalMemory(forceFullCollection: true);
            Console.WriteLine($"Memory Usage: {memoryUsed / (1024.0 * 1024.0)} MB");
        }

        private static void UsingDeclaration()
        {
            //This variable will be in scope until the end of the method
            using var rw = new MyResourceWrapper();
            //Do something here
            Console.WriteLine("About to dispose.");
            //Variable is disposed at this point.
        }

        static void CreateObjects(int count)
        {
            MyResourceWrapper[]? tonsOfObjects = new MyResourceWrapper[count];
            for (int i = 0; i < count; i++)
            {
                tonsOfObjects[i] = new MyResourceWrapper();
            }
            tonsOfObjects = null;
        }

        static void DisposeFileStream()
        {
            FileStream fs = new FileStream("myFile.txt", FileMode.OpenOrCreate);
            // Confusing, to say the least!
            // These method calls do the same thing!
            fs.Close();
            fs.Dispose();
        }

        // I'll draw anyone supporting IDraw3D.
        static void DrawIn3D(IDraw3D itf3d)
        {
            Console.WriteLine("-> Drawing IDraw3D compatible type");
            itf3d.Draw3D();
        }
    }

    class SortPeopleByAge : IComparer<Person>
    {
        public int Compare(Person? firstPerson, Person? secondPerson)
        {
            if (firstPerson?.Age > secondPerson?.Age)
            {
                return 1;
            }
            if (firstPerson?.Age < secondPerson?.Age)
            {
                return -1;
            }
            return 0;
        }
    }

    class Person
    {
        /// <summary>
        /// field
        /// </summary>
        private int? _personAge;
        private string? _personName;
        public string? PersonName => _personName;

        public int Age { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        // public Person()
        // {
        //     Console.WriteLine("Hi");
        // }

        // public Person(string? personName, int? personAge) : this()
        // {
        //     _personName = personName;
        //     _personAge = personAge;
        // }

        public Person() { }
        public Person(string firstName, string lastName, int age)
        {
            Age = age;
            FirstName = firstName;
            LastName = lastName;
        }

        public override string ToString()
        {
            return $"Name: {FirstName} {LastName}, Age: {Age}";
        }

        public virtual void Talk()
        {
            Console.WriteLine("I'm person!");
        }

        public override bool Equals(object? obj)
        {
            return obj is Person other && other.PersonName == _personName;
        }

        public override int GetHashCode()
        {
            return _personAge.GetHashCode();
        }

        public bool Equals(Person other)
        {
            return other.PersonName == _personName;
        }

        // public virtual int _personAge()
        // {
        //     return _personAge;
        // }
    }

    // struct Point
    // {
    //     public Point(int x, int y)
    //     {
    //         X = x;
    //         Y = y;
    //     }

    //     public int X { get; set; }
    //     public int Y { get; set; }

    //     public override string ToString()
    //     {
    //         return $"{X} {Y}";
    //     }
    // }

    // sealed class Employee : Person
    // {
    //     /// <summary>
    //     /// property
    //     /// </summary>
    //     public int Age { get; set; }
    //     public string Name { get; private set; }
    //     private string? EmpTalk;

    //     /// <summary>
    //     /// compute property
    //     /// </summary>
    //     public string? EmpSpeak
    //     {
    //         get => EmpTalk;
    //         // set => Speak = value; // no condition
    //         set
    //         {
    //             if (string.IsNullOrEmpty(value))
    //             {
    //                 EmpTalk = "Hi";
    //             }
    //             else
    //             {
    //                 EmpTalk = value;
    //             }
    //         } //condition
    //     }

    //     /// <summary>
    //     /// constructor
    //     /// </summary>
    //     /// <param name="talk"></param>
    //     /// <param name="age"></param>
    //     public Employee(string? talk, int age, string name)
    //     {
    //         EmpSpeak = talk ?? "";
    //         Age = age;
    //         Name = name;
    //     }

    //     public void SetEmpName(string name)
    //     {
    //         Name = name;
    //     }

    //     public override void Talk()
    //     {
    //         Console.WriteLine(EmpTalk);
    //     }

    //     // public override string ToString()
    //     // {
    //     //     Talk();
    //     //     return $"Defaule age: {_personAge()} \nAge now: {Age} \nName: {Name}";
    //     // }
    // }
}