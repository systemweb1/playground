namespace playground.Enum
{
    public enum GCCollectionMode
    {
        Default, // Forced is the current default.
        Forced, // Tells the runtime to collect immediately!
        Optimized // Allows the runtime to determine whether the current time is optimal to reclaim objects.
    }

    public enum NotifyCollectionChangedAction
    {
        Add = 0,
        Remove = 1,
        Replace = 2,
        Move = 3,
        Reset = 4,
    }
}